import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// importar modulos externos de primeNg
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { CalendarModule } from 'primeng/calendar';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MenubarModule } from 'primeng/menubar';
import { MessagesModule } from 'primeng/messages';
import { MenuModule } from 'primeng/menu';
import { MultiSelectModule } from 'primeng/multiselect';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { PasswordModule } from "primeng/password";
import { RatingModule } from 'primeng/rating';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { TooltipModule } from 'primeng/tooltip';
@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  // El array imports contiene los módulos que se implementará en todos los componente
  exports: [
    AutoCompleteModule,
    ButtonModule,
    CardModule,
    CalendarModule,
    CardModule,
    ConfirmDialogModule,
    DropdownModule,
    DialogModule,
    InputTextModule,
    InputTextareaModule,
    MenubarModule,
    MessagesModule,
    MenuModule,
    MultiSelectModule,
    PanelModule,
    PaginatorModule,
    PasswordModule,
    RatingModule,
    TableModule,
    TabViewModule,
    TooltipModule
  ],
  // El array provider devuelve servicios.
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class PrimeNgModule { }
