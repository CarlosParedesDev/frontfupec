import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// modulos
import { PrimeNgModule } from '../primeNg/prime-ng.module';
import { SharedModule } from '../shared/shared.module';

// componentes
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    LoginComponent
  ],
  // El array imports contiene los módulos que se implementará en el componente login
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    PrimeNgModule,
    SharedModule
  ]
})
export class AuthModule { }
