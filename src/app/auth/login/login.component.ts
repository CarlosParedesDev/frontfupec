/* El archivo typesCript es el lugar donde se maneja la lógica del componente login,
en la cuál implementamos variables, grupo de controles en un formulario, métodos,
ciclos de vida, etc, los cuales nos facilitarán el envio de la información a la vista */

import { Component, OnInit } from '@angular/core';
// Formulario reactivo
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// Rutas
import { Router } from '@angular/router';
// Modelos
import { User } from '../../models/user.model';
import { User_groups } from '../../models/user_groups.model';
// Services
import { AuthService } from '../../service/Auth/auth.service';
import { MensajesSweerAlertService } from '../../service/mensajes-sweer-alert.service';
import sha1 from '../sha1/sha1';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;
  formSubmitted = false;

  // usuario a autentificar
  usuario!: User;

  // Roles
  roles: string [] = ['ROLE_Admin','ROLE_User'];
  // Constructor con inyección de dependencias
  constructor(
    private formBuider: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private mensajesService: MensajesSweerAlertService
  ) { }

  // Ciclos de vida ngOnInit
  ngOnInit(): void {
    // formBuider me permite construir un formulario creando un grupo de controles
    this.cargarFormulario();
    // vericamos autentificacion
    this.verificarUsuarioLogIn();
  }

  cargarFormulario = (): void => {
    this.loginForm = this.formBuider.group({
      username: ['', [Validators.required, Validators.minLength(2)]],
      password:  [ '', [Validators.required,  Validators.minLength(2)]]
    });
  }

  // verificamos si ya esta autentificado
  verificarUsuarioLogIn = () => {
    if ( this.authService.isAuthenticated()){
      this.mensajesService.mensajeSweetInformacionToast('info', `${this.authService.usuario.username} , ya estas autentificado`);     
      this.router.navigate(['/tarjeta/:page']);
    } else {
      this.router.navigate(['/login']);
    }
  }
  enviarDatosLogin = (): void => {
    this.formSubmitted = true;
    // verificamos formulario valido
    if (this.loginForm.valid) {
      let {username, password} = this.loginForm.value;
      let userLevel: User_groups = {id:0,groupName: ''}     
      let passwordSha1 = sha1(password); // encriptamos en hash sha1 
      this.usuario = {id:0, name:'', username, password: passwordSha1, userLevel};
      // pasamos datos a enviar
      this.iniciarSession(this.usuario);
    }
  }

  // mandamos datos usuario
  iniciarSession = (usuario: User): void => {
    if ( !usuario.username || ! usuario.password) {
      return;
    }
    this.authService.iniciarSesion(usuario).subscribe( response => {
                                              // guardamos datos de la autentificacion
                                              this.authService.guardarUsuario(response.access_token);
                                              // @ts-ignore // para ver si tiene roles correctos
                                              if (!this.roles.includes(this.authService.usuario.userLevel[0])){
                                                this.mensajesService.mensajeSweetInformacionToast('warning', 
                                                                                                  'No tienes acceso al sistema');
                                                return;
                                              }
                                              // si esta correcto guardamos token y damos acceso a la pagina principal
                                              this.authService.guardarToken(response.access_token);
                                              this.mensajesService.mensajeSweetInformacionToast('info', 
                                                                                                `Bienvenido, ${this.authService.usuario.username}`);
                                              this.router.navigate(['/tarjeta/:page']);
                                            }, error => {
                                              // datos invalidos
                                              if ( error.status === 400) {
                                                this.mensajesService.mensajeSweetInformacion('warning', 'Usuario o contraseñas incorrectas');
                                              }
                                            })
  }


}
