/**
 * Guardian para revisar si el token no a expirado y el usuario este autentificado
 */
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

// servicios
import { AuthService } from '../service/Auth/auth.service';
import { MensajesSweerAlertService } from '../service/mensajes-sweer-alert.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService,
    private mensajeService: MensajesSweerAlertService
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.authService.isAuthenticated()) {
        // ver si el token esta disponible y si no ha expirado
        if (this.isTokenExpired()) {
          this.authService.cerrarSesion();
          this.mensajeService.mensajeSweetInformacionToast('info','Expiró la sesión');
          this.router.navigate(['/login']);
          return false;
        }
        return true;
      }
      this.router.navigate(['/login']);
      return false;
  }

  // verifica si el token expira
  isTokenExpired(): boolean {
    let token = this.authService.token;
    let payload = this.authService.obtenerDatosToken(token);
    // extraemos el tiempo actual en milisegundos
    let now = new Date().getTime() / 1000;
    // comparamos la fecha actual con el token guardado
    if (payload.exp < now) {
      return true;
    }
    return false;
  }
  
}
