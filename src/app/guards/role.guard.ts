/**
 * Guardian para revisar los roles del usuario ademas si este esta autentificado
 */
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

// servicios
import { AuthService } from '../service/Auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService,
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      // verificamos que el rol del usuario sea el correcto para permitir el acceso a las ventanas del app
      if (!this.authService.isAuthenticated()) {
        this.router.navigate(['/login']);
        return false;
      }
      let role = route.data['role'] ;            
      for (let i = 0; i < role.length; i++) {                
        if (this.authService.hasRole(role[i])) {          
          return true;
        }
      }
      // si no tiene ningun rol requerido cerramos session
      this.authService.cerrarSesion();
      this.router.navigate(['/login']);
      return false;
  }
  
}
