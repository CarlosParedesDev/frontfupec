import { Component, OnInit } from '@angular/core';

// serivicios
import { AuthService } from './service/Auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'segumientoAngular';
  constructor(
    public authService: AuthService
  ) {}
  ngOnInit = () => {
  }
}
