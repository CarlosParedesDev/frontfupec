/* En la  interfaz tenemos declarado los atributos de la clase Interes */
export interface Interes {
    id: number;
    nombre: string;
    descripcion: string;
    eliminado: boolean;
    fechaGrabado: Date;
}
