/**
 * modelo para un reporte
 */
export interface VendedorReporte {
    vendedor:       string;
    clientes:       number;
    clientesAct:    number;
    clientesDes:    number;
    promNivel:      string |number;
}