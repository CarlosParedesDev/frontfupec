// modelos para pasar datos al momento de crear un reporte
export interface GenerarReporte {
    autor: string;
    titulo: string;
    parametros: string;
    nombreArch: string;
    columnas: any[];
    datos: any[];
}
export interface GenerarReporteXlsx {
    autor: string;
    nombreArch: string;
    titulo: string,
    cabeceras: string[];
    datos: any[];
    parametros?: {
        titulo: string,
        fechas: string,
        nivel: string
        opcion: number
    }; 
}