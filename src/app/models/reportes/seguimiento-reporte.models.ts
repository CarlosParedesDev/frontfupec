/**
 * modelo para un reporte
 */
export interface SeguimientoReporte {
    fecha: string;
    cliente: string;
    tarjeta: string;
    vendedor: string;
    nivelSatisfaccion: number;
    observacion: string;
    razonInconveniente: string;
    sugerenciaCliente: string;
}