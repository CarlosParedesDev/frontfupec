/* En la  interfaz tenemos declarado los atributos de la clase Usuario */
export interface Usuario {
    id: number;
    usuario: string;
    clave: string;
    nombre: string;
    telefono: string;
}