import { User_groups } from './user_groups.model';
/* En la  interfaz tenemos declarado los atributos de la clase User */
export interface User {
    id: number;
    name: string;
    username: string;
    password: string;
    userLevel: User_groups;
    image?: string;
    lastLogin?: Date;
    status?: number;
}