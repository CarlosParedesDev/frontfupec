/* La interfaz declara los atributos de la clase Response */
export interface Response {
    ok: string;
    mensaje: string;
    respuesta: any;
}
/* En la  interfaz tenemos declarado los atributos de la clase ResponseValidacion */
export interface ResponseValidacion {
    ok: string;
    mensaje: string;
    error: string;
}