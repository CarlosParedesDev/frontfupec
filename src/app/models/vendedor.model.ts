import { Cliente } from './cliente.models';
/* En la  interfaz tenemos declarado los atributos de la clase Vendedor */
export interface Vendedor {
    id: number;
    nombres: string;
    apellidos: string;
    email: string;
    telefono: string;
    fechaGrabado: Date;
    eliminado: boolean;
    clientes?: Cliente [];
}