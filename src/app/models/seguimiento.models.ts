import { Cliente } from './cliente.models';
import { Usuario } from './usuario.models';
import { User } from './user.model';

/* En la  interfaz tenemos declarado los atributos de la clase Seguimiento */
export interface Seguimiento {
    id: number;
    fecha: Date;
    observacion: string;
    nivelSatisfaccion: number;
    razonInconveniente: string;
    sugerenciaCliente: string;
    user: User;
    cliente: Cliente;
    eliminado: boolean;
    fechaGrabado?: Date;
}