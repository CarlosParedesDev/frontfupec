/* En la  interfaz tenemos declarado los atributos de la clase Tarjeta */
export interface Tarjeta {
    id: number;
    codigo: string;
    activo: boolean;
    fechaGrabado: Date;
    fechaCaducar: Date;
}