
import { Tarjeta } from "./tarjeta.model";
import { Usuario } from "./usuario.models";
import { Vendedor } from "./vendedor.model";
import { Seguimiento } from './seguimiento.models';
import { Interes } from './interes.models';
import { User } from './user.model';
/* En la  interfaz tenemos declarado los atributos de la clase Cliente */
export interface Cliente {
        id: number;
        cedula: string;
        genero: string;
        ocupacion: string;
        tipoSangre: string;
        puntos: number;
        eliminado: boolean;
        usuario: Usuario;
        tarjeta: Tarjeta;
        vendedor: Vendedor;
        user: User;
        seguimientos: Seguimiento [];
        intereses: Interes [];
        fechaGrabado?: Date;
}