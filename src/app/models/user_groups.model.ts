/* En la  interfaz tenemos declarado los atributos de la clase User_groups */
export interface User_groups {
    id: number;
    groupName: string;
    groupLevel?: number;
    groupStatus?: number;
}