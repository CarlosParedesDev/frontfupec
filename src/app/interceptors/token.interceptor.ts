import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../service/Auth/auth.service';

@Injectable()
/**
 * Interceptor para pasar por cada peticion de usuario el token actual
 * 
 */
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let token = this.authService.token;
        if (token != null) {
            const authReq = request.clone({
                headers: request.headers.set('Authorization', 'Bearer ' + token)
            });
            return next.handle(authReq);
        }
    return next.handle(request);
  }
}
