import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

// servicios
import { AuthService } from '../service/Auth/auth.service';
import { MensajesSweerAlertService } from '../service/mensajes-sweer-alert.service';

@Injectable()
/**
 * Interceptor para revisar si el usuario tiene la sesion valida
 * Ademas ver si tiene acceso a las peticiones que haga desde el front
 */
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    private authService: AuthService,
    private mensajeService: MensajesSweerAlertService
    ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {    
    return next.handle(request).pipe(
                                  catchError(e => {
                                      // no esta autentificado
                                      if (e.status == 401) {
                                          if (this.authService.isAuthenticated()) {
                                              this.authService.cerrarSesion();
                                          }
                                          this.router.navigate(['/login']);
                                      }
                                      // no tiene acceso a los endpoints
                                      if (e.status == 403) {
                                          this.mensajeService.mensajeSweetInformacionToast('warning','No tienes acceso a este recurso')
                                          this.router.navigate(['/tarjeta/:page']);
                                      }
                                      return throwError(e);
                                  })
                                );
  }
}
