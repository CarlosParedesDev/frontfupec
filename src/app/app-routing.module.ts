import { NgModule } from '@angular/core';
// Ruta
import { RouterModule, Routes } from '@angular/router';

// guards 
import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';

// componentes
import { AgregarClienteComponent } from './pages/agregar-cliente/agregar-cliente.component';
import { AgregarInteresComponent } from './pages/agregar-interes/agregar-interes.component';
import { AgregarSeguimientoComponent } from './pages/agregar-seguimiento/agregar-seguimiento.component';
import { AgregarTarjetaComponent } from './pages/agregar-tarjeta/agregar-tarjeta.component';
import { AgregarVendedorComponent } from './pages/agregar-vendedor/agregar-vendedor.component';
import { ClienteCrudComponent } from './pages/cliente-crud/cliente-crud.component';
import { DetalleClienteComponent } from './pages/detalle-cliente/detalle-cliente.component';
import { InteresCrudComponent } from './pages/interes-crud/interes-crud.component';
import { LoginComponent } from './auth/login/login.component';
import { ObservcionesComponent } from './pages/observciones/observciones.component';
import { ReporteSeguimientoComponent } from './pages/reporte-seguimiento/reporte-seguimiento.component';
import { ReporteVendedorComponent } from './pages/reporte-vendedor/reporte-vendedor.component';
import { SeguimientoClienteCrudComponent } from './pages/seguimiento-cliente-crud/seguimiento-cliente-crud.component';
import { SugerenciaComponent } from './pages/sugerencia/sugerencia.component';
import { TarjetaCrudComponent } from './pages/tarjeta-crud/tarjeta-crud.component';
import { TarjetaCaducidadComponent } from './pages/tarjeta-caducidad/tarjeta-caducidad.component';
import { VendedorCrudComponent } from './pages/vendedor-crud/vendedor-crud.component';

// array de objetos que contiene la ruta y el componente
const routes: Routes = [
  { // redirecciona al login
    path: 'login',
    component: LoginComponent
  },
  { // redirecciona al crud tarjeta
    path: 'tarjeta/:page',
    component: TarjetaCrudComponent,
    canActivate:[ AuthGuard, RoleGuard ],// pasamos los guards para revisar el usuario autentificado
    data:{ role: ['ROLE_Admin', 'ROLE_User']}// pasamos los roles posibles que puede aceptar la ruta
  },
  { // redirecciona al crud vendedor
    path: 'vendedor/:page',
    component: VendedorCrudComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al crud cliente
    path: 'cliente/:page',
    component: ClienteCrudComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al crud interes
    path: 'interes/:page',
    component: InteresCrudComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al crud seguimiento
    path: 'seguimientoCliente/:page',
    component: SeguimientoClienteCrudComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al reporte detalle cliente
    path: 'detalleCliente',
    component: DetalleClienteComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al reporte tarjeta caducidad
    path: 'tarjetaCaducidad',
    component: TarjetaCaducidadComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al reporte observación
    path: 'observacion',
    component: ObservcionesComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al reporte sugerencia
    path: 'sugerencia',
    component: SugerenciaComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al reporte seguimiento
    path: 'reporteSeguimiento',
    component: ReporteSeguimientoComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al reporte detalle cliente
    path: 'vendedorCliente',
    component: ReporteVendedorComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al formulario tarjeta
    path: 'agregarTarjeta/:id',
    component: AgregarTarjetaComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al formulario vendedor
    path: 'agregarVendedor/:id',
    component: AgregarVendedorComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al formulario cliente
    path: 'agregarCliente/:id',
    component: AgregarClienteComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al formulario interes
    path: 'agregarInteres/:id',
    component: AgregarInteresComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // redirecciona al formulario seguimiento
    path: 'agregarSeguimiento/:idc/:ids',
    component: AgregarSeguimientoComponent,
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  },
  { // comodin q me direcione a la pagina principal
    path: '**',
    redirectTo: '',
    canActivate:[ AuthGuard, RoleGuard ],
    data:{ role: ['ROLE_Admin', 'ROLE_User']}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
