import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// primeng
import { PrimeNgModule } from '../primeNg/prime-ng.module';
import { SharedModule } from '../shared/shared.module';
// componentes
import { AgregarClienteComponent } from './agregar-cliente/agregar-cliente.component';
import { AgregarInteresComponent } from './agregar-interes/agregar-interes.component';
import { AgregarSeguimientoComponent } from './agregar-seguimiento/agregar-seguimiento.component';
import { AgregarTarjetaComponent } from './agregar-tarjeta/agregar-tarjeta.component';
import { AgregarVendedorComponent } from './agregar-vendedor/agregar-vendedor.component';
import { ClienteCrudComponent } from './cliente-crud/cliente-crud.component';
import { DetalleClienteComponent } from './detalle-cliente/detalle-cliente.component';
import { InteresCrudComponent } from './interes-crud/interes-crud.component';
import { ObservcionesComponent } from './observciones/observciones.component';
import { ReporteSeguimientoComponent } from './reporte-seguimiento/reporte-seguimiento.component';
import { ReporteVendedorComponent } from './reporte-vendedor/reporte-vendedor.component';
import { SeguimientoClienteCrudComponent } from './seguimiento-cliente-crud/seguimiento-cliente-crud.component';
import { SugerenciaComponent } from './sugerencia/sugerencia.component';
import { TarjetaCrudComponent } from './tarjeta-crud/tarjeta-crud.component';
import { TarjetaCaducidadComponent } from './tarjeta-caducidad/tarjeta-caducidad.component';
import { VendedorCrudComponent } from './vendedor-crud/vendedor-crud.component';



@NgModule({
  declarations: [
    AgregarClienteComponent,
    AgregarInteresComponent,
    AgregarTarjetaComponent,
    AgregarSeguimientoComponent,
    AgregarVendedorComponent,
    ClienteCrudComponent,
    DetalleClienteComponent,
    InteresCrudComponent,
    ObservcionesComponent,
    ReporteSeguimientoComponent,
    ReporteVendedorComponent,
    SeguimientoClienteCrudComponent,
    SugerenciaComponent,
    TarjetaCrudComponent,
    TarjetaCaducidadComponent,
    VendedorCrudComponent,
  ],
  // El array imports contiene los módulos que se implementará en todos los componente
  imports: [
    CommonModule,
    PrimeNgModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule
  ],
  // El array exports me permite exportar los componentes.
  exports: [
    ClienteCrudComponent,
    DetalleClienteComponent,
    InteresCrudComponent,
    ObservcionesComponent,
    ReporteSeguimientoComponent,
    ReporteVendedorComponent,
    SugerenciaComponent,
    TarjetaCaducidadComponent,
    TarjetaCrudComponent,
    SeguimientoClienteCrudComponent,
    VendedorCrudComponent,
  ]
})
export class PagesModule { }

