/* El archivo typesCript es el lugar donde se maneja la lógica del componente vendedor-crud,
en la cuál implementamos variables, grupo de controles en un formulario, métodos,
ciclos de vida, etc, los cuales nos facilitarán el envio de la información a la vista */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
// Ruta
import { ActivatedRoute, Router } from '@angular/router';
// pimeng
import { ConfirmationService } from 'primeng/api';
// modelos
import { Vendedor } from '../../models/vendedor.model';
import { Response, ResponseValidacion } from '../../models/datos.models';
// services
import { AuthService } from '../../service/Auth/auth.service';
import { MensajesSweerAlertService } from '../../service/mensajes-sweer-alert.service';
import { VendedorService } from '../../service/vendedor.service';

@Component({
  selector: 'app-vendedor-crud',
  templateUrl: './vendedor-crud.component.html',
  styleUrls: ['./vendedor-crud.component.css']
})

export class VendedorCrudComponent implements OnInit, OnDestroy{

  listaVendedores: Vendedor [] = [];
  paginador!: any;
  num = 0;
 
  // para poder cerrar subscripciones
  urlSubscription!: Subscription;
  urlSubscriptionBus!: Subscription;

  seleccionCampo!: string;
  terminoBusqueda: string =''; 
  // verificar campos para busqueda
  verificarCampo: boolean = false;
  verificarTermino: boolean = false;

  // mensajes de error
  mensajeError!: string;

  // comboBox para realizar busquedas
  buscarPor = [
    {label: 'Nombre', value: 'nombres'},
    {label: 'Apellido', value: 'apellidos'},
    {label: 'Email', value: 'email'}
    ];

  // Constructor con inyección de dependencias
  constructor(
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private confirmationService: ConfirmationService,
    public authService: AuthService,
    private vendedorService: VendedorService,
    private mensajesService: MensajesSweerAlertService
  ){
  }

  // Ciclos de vida ngOnInit
  ngOnInit(): void {
    this.listaVendedoresPag();
  }

  // Ciclos de vida ngOnDestroy
  ngOnDestroy(): void {
    this.cerrarSubscripcionLista();
    this.cerrarSubscripcionUrlBus();
  }

  // Método para cambiar de paginacion
  listaVendedoresPag = (): void => {
    // si existe otra supscripcion a params cerramos
    this.cerrarSubscripcionUrlBus();
    this.urlSubscription = this.activatedRouter.paramMap.subscribe( params => {
      // leemos y validamos el parametro url 
      //@ts-ignore
      let pagina = +params.get('page');
      if ( !pagina) {
        pagina = 0;
      }
      this.num = pagina;
      // metodo del servicio para listar
      this.vendedorService.listaVendedores(pagina)
        .subscribe( (response: any) => {
            if (response.empty) {
              this.listaVendedores = [];
              // metodo del servicio para listar
              this.paginador = null;
              this.router.navigateByUrl('vendedor/:page');
              this.cerrarSubscripcionLista();
              return;
            }
            // el dato emitido va a ser de tipo Vendedor
            this.listaVendedores = (response.content as Vendedor[]);
            this.paginador = response;
        }, error => {
          this.cerrarSubscripcionLista();
          if (error.status === 0 ) this.mensajeError = 'Error de conección con el servidor';
        }
      );
    });
  }

  // Método para eliminar un vendedor mediante un id
  eliminarVendedor = (vendedor: Vendedor) => {
    this.cerrarSubscripcionLista();
    this.cerrarSubscripcionUrlBus();
    // para el confirm dialog de primeng
    this.confirmationService.confirm({
        header: '¿Confirmar esta acción?',
        message: `Desea eliminar al vendedor: ${vendedor.nombres} ${vendedor.apellidos}`,
        icon: 'pi pi-info-circle',
        acceptLabel: 'Si',
        accept: () => {
            if( vendedor ) {
              // eliminamos vendedor
              this.vendedorService.borrarVendedor(vendedor.id)
                  .subscribe( (response: Response) => {
                    this.mensajesService.mensajeSweetInformacionToast('success', response.mensaje);                    
                    // pasos para ver si existe mas de un dato si ahy una busqueda
                    if ( this.terminoBusqueda.length > 0 && this.paginador.totalPages >= 1 && this.paginador.totalElements > 1 ) { 
                      this.filtrarVendedores();
                      return;
                    } 
                    // si el array contiene un 1 se regresa al inicio
                    if ( this.paginador.totalPages === 1 && this.paginador.totalElements === 1 ) {
                      this.terminoBusqueda = '';
                      this.seleccionCampo = '';
                      this.listaVendedoresPag();
                      return;
                    }
                    this.listaVendedoresPag();
                  }, error => {
                    // mandamos mesajes de error al sweetAlert
                    const errores = error.error as ResponseValidacion;
                    this.mensajesService.mensajeSweetFire('error', errores.error, errores.mensaje);
                    this.listaVendedoresPag();
                    this.terminoBusqueda = '';
                    this.seleccionCampo = '';
                  }
                );
            }
        }
    });
  }

  // Método para enrutar al inicio del paginador
  verificarSeleccionCampo = (): void => {
    this.verificarCampo = false;
    this.router.navigateByUrl('vendedor/0');
  }

  // Método para filtrar la busqueda
  filtrarVendedores = ( ): void => {
    let termino = this.terminoBusqueda.trim();
    // si existe otra supscripcion a params cerramos
    this.cerrarSubscripcionLista();
    this.cerrarSubscripcionUrlBus();
    // validamos campos de busqueda
    if ( termino.length === 0) {
      this.listaVendedoresPag();
      this.verificarTermino = true;
      return;
    } else if ( !this.seleccionCampo) {
      this.verificarTermino = false;
      this.verificarCampo = true;
      return;
    }
    this.verificarTermino = false;
    // busqueda para filtrar por campo
    this.urlSubscriptionBus = this.activatedRouter.paramMap.subscribe( params => {
    // leemos y validamos el parametro url
    // @ts-ignore
      let pagina = +params.get('page');
      if ( !pagina || this.listaVendedores.length <= 1 ) {
        pagina = 0;
      }
      // método del servicio para buscar
      this.vendedorService.listaVendedoresBusqueda(pagina, this.terminoBusqueda, this.seleccionCampo)
          .subscribe( (response: Response) => {
            this.listaVendedores = (response.respuesta.content as Vendedor[]);
            this.paginador = response.respuesta;
          }, error => {
            // mensaje
            this.mensajesService.mensajeSweetInformacion('info', `No hay ${this.seleccionCampo} con : "${this.terminoBusqueda}"`);
            this.terminoBusqueda = '';
            this.seleccionCampo = '';
            this.listaVendedoresPag();
          });
    });
    this.cerrarSubscripcionLista();
  }
  
  // Método para cerrar la Subscripción
  cerrarSubscripcionLista = (): void => {
    if ( this.urlSubscription ) this.urlSubscription.unsubscribe();
  }
  
  // Método para cerrar la Subscripción
  cerrarSubscripcionUrlBus = (): void => {
    if ( this.urlSubscriptionBus ) this.urlSubscriptionBus.unsubscribe();
  }
}
