/* El archivo typesCript es el lugar donde se maneja la lógica del componente seguimiento,
en la cuál implementamos variables, grupo de controles en un formulario, métodos,
ciclos de vida, etc, los cuales nos facilitarán el envio de la información a la vista */
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
// Formulario reactivo
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// Rutas
import { ActivatedRoute, Router } from '@angular/router';
// models
import { Response } from '../../models/datos.models';
import { Cliente } from 'src/app/models/cliente.models';
import { Seguimiento } from 'src/app/models/seguimiento.models';
import { User } from '../../models/user.model';
import { User_groups } from '../../models/user_groups.model';
// services
import { ClienteService } from '../../service/cliente.service';
import { MensajesSweerAlertService } from '../../service/mensajes-sweer-alert.service';
import { SeguimientoService } from '../../service/seguimiento.service';
import { AuthService } from '../../service/Auth/auth.service';

@Component({
  selector: 'app-agregar-seguimiento',
  templateUrl: './agregar-seguimiento.component.html',
  styleUrls: ['./agregar-seguimiento.component.css']
})

export class AgregarSeguimientoComponent implements OnInit {
  erroesBack: string[] = [];
  formSubmitted = false; // para ver si se ha realizado alguna accion en el form
  seguimientoForm!: FormGroup;
  seguimiento!: Seguimiento;
  seguimientoSeleccionado!: Seguimiento;
  clienteSeleccionado!: Cliente;

  // modal bandera
  abrir = false;
  // badera para mostrar campo filtrado
  verCampoFiltrado = false;

  // <------------------ lista nivel de satisfaccion ----------------------->
  nivelSatisfaccionCliente = [
    { label: '1', value: 1 },
    { label: '2', value: 2 },
    { label: '3', value: 3 },
    { label: '4', value: 4 },
    { label: '5', value: 5 }
  ];

  // campos a filtrar
  listaClientes: Cliente[] = [];
  seleccionCampo!: string;
  
  // <------------------ lista del comboBox ----------------------->
  buscarPor = [
    { label: 'Cedula', value: 'cedula' },
    { label: 'Nombre', value: 'nombre' }
  ];

  // Constructor con inyección de dependencias
  constructor(
    private formBuider: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private clienteService: ClienteService,
    private mensajesService: MensajesSweerAlertService,
    private seguimientoService: SeguimientoService,
  ) { }

  // Ciclos de vida ngOnInit
  ngOnInit(): void {
    // buscamos cliente por id parametro
    this.BuscarClientePorIdParametro();
    // Formulario reactivo
    this.crearFormularioCliente();
    // this.asignarCliente(this.clienteSeleccionado);
  }

  // Método donde tengo el FormGroup con sus validaciones
  crearFormularioCliente = (): void => {
    // formBuider me permite construir un formulario creando un grupo de controles
    this.seguimientoForm = this.formBuider.group({
      observacion: ['', [Validators.required, Validators.minLength(2)]],
      nivelSatisfaccion: ['', [Validators.required]],
      razonInconveniente: ['', [Validators.required, Validators.minLength(2)]],
      sugerenciaCliente: ['', [Validators.required, Validators.minLength(2)]],
      fecha: [ new Date() , [Validators.required]]
    });
  }

  // Método donde verificamos si hay parametro id valido por url
  BuscarClientePorIdParametro = (): void => {
    this.activatedRoute.paramMap
      .subscribe(params => {
        // @ts-ignore
        let id = +params.get('idc');
        if (id) {
          this.clienteService.buscarPorIdCliente(id)
            .subscribe((response: Response) => {
              this.verCampoFiltrado = true;
              // el dato emitido va a ser de tipo Cliente
              this.clienteSeleccionado = response.respuesta as Cliente;
            }, error => {
              // si da error regresamos a la tabla
              this.mensajesService.mensajeSweetInformacion('warning', error.error.mensaje);
              this.router.navigateByUrl('seguimientoCliente/:page');
            });
        } else { 
          // verificamos si hay id de un seguimiento         
          this.BuscarSeguimientoPorIdParametro();
        }
      });
  }

    // Método donde buscamos si hay parametro ids valido por url
    BuscarSeguimientoPorIdParametro = (): void => {
      this.activatedRoute.paramMap
        .subscribe(params => {
          // @ts-ignore
          let id = +params.get('ids');
          if (id) {
            this.seguimientoService.buscarPorIdSeguimiento(id)
              .subscribe((response: Response) => {
                this.verCampoFiltrado = true;
                // el dato emitido va a ser de tipo Seguimiento
                this.seguimientoSeleccionado = response.respuesta as Seguimiento;
                this.clienteSeleccionado = this.seguimientoSeleccionado.cliente;
                // Desestructuración De Objetos                
                let { observacion, nivelSatisfaccion, razonInconveniente, sugerenciaCliente, fecha} = this.seguimientoSeleccionado;
                fecha = new Date(fecha);  
                // pasamos valores al formulario reactivo
                this.seguimientoForm
                    .setValue({ observacion, nivelSatisfaccion, razonInconveniente, sugerenciaCliente, fecha });
              }, error => {
                // si da error regresamos a la tabla
                this.mensajesService.mensajeSweetInformacion('warning', error.error.mensaje)
                this.router.navigateByUrl('seguimientoCliente/:page');
              });
          } 
        });
    }
  // Método para extarer informacion del formulario y mandar a guardar o actualizar los seguimientos
  enviarDatos = (): void => {
    if (!this.clienteSeleccionado) return this.mensajesService.mensajeSweetInformacion('warning', 'Debe seleccionar un cliente');
    this.formSubmitted = true;
    this.erroesBack = [];
    // Desestructuración De Objetos
    let { observacion, nivelSatisfaccion, razonInconveniente, sugerenciaCliente, fecha } = this.seguimientoForm.value;
    // para enviar el usuario autor
    let {id, name, username, password} = this.authService.usuario;
    let userLevel: User_groups = {id:0, groupName:''}
    let user: User = {id, name, username, userLevel, password};
    this.seguimiento = {  id: 0, fecha, observacion, nivelSatisfaccion, 
                          razonInconveniente, sugerenciaCliente, cliente: this.clienteSeleccionado, 
                          user ,eliminado: true};
    if (this.seguimientoSeleccionado && this.seguimientoForm.valid) {
      this.actualizarSeguimiento(this.seguimientoSeleccionado.id);
    } else if (this.seguimientoForm.valid) {
      this.crearSeguimiento();
    }
  }

  // Método para crear seguimientos
  crearSeguimiento = (): void => {    
    this.seguimientoService.crearSeguimiento(this.seguimiento)
      .subscribe(response => {
        let data = response as Response;
        this.mensajesService.mensajeSweetInformacion('success', data.mensaje);
        this.limpiarCamposFormulario();
      }, error => {
        this.erroresBackEnd(error);
      });
  }

  // Método para actualizar seguimientos
  actualizarSeguimiento = (id: number): void => {
    this.seguimientoService.actualizarSeguimiento(id, this.seguimiento)
        .subscribe( (response: Response) => {
          let data = response as Response;
          this.mensajesService.mensajeSweetInformacion('success', data.mensaje);
          this.limpiarCamposFormulario();
          this.router.navigateByUrl('seguimientoCliente/:page');
        }, error => this.erroresBackEnd(error));
  }

  // Método donde guardamos los posibles erroes del back
  erroresBackEnd = (error: any): void => {    
    if (error.status === 400) { this.erroesBack.push('Hay campos vacios'); }
    if (error.errores) {
      this.erroesBack = error.errores;
    } else {
      this.erroesBack.push(error.mensaje);
    }
  }

  // Método para resetar campos del formulario
  limpiarCamposFormulario = (): void => {
    this.seguimientoForm.reset();
    // this.seguimientoForm.setValue({fecha: new Date()})
    this.seguimientoForm.get('fecha')?.setValue(new Date());
    this.formSubmitted = false;
  }

  // Método para abrir modal
  abrirModal = () => this.abrir = true;

  // Método para asignar un dato al campo
  verificarSeleccionCampo = (event: any) => this.seleccionCampo = event.value;

  //  Método para listar vendedor, tarjetas por termino -------------------->
  listarDatos = (event: any): void => {
    let termino = event.query.trim();
    // verificamos si esta seleccionado un campo para el filtrado
    if (!this.seleccionCampo) {
      termino = '';
      return;
    }
    if (termino.length == 0) {
      this.listaClientes = [];
      termino = '';
      return;
    }
    // verificamos el tipo de filtrado
    this.seleccionCampo === 'cedula' ? this.listaClientesCedula(termino) : this.listaClientesNombre(termino);
  }

  // Método para listar clientes por cedula en el input de filtrado
  listaClientesCedula = (termino: string): void => {
    this.clienteService.listarClientesFiltrado(this.seleccionCampo, termino)
      .subscribe((response: Response) => {
        this.listaClientes = response.respuesta as Cliente[];
      },error => {
        this.listaClientes = [];
      });
  }

  // Método para listar clientes por nombre en el input de filtrado
  listaClientesNombre = (termino: string): void => {
    this.clienteService.listarClientesFiltrado(this.seleccionCampo, termino)
      .subscribe((response: Response) => {
        this.listaClientes = response.respuesta as Cliente[];
      }, error => {
        this.listaClientes = [];
      });
  }

  // Método para asignar un cliente por medio del filtrado
  asignarCliente = (cliente: Cliente) => this.clienteSeleccionado = cliente;

}
