/* El archivo typesCript es el lugar donde se maneja la lógica del componente interes-crud,
en la cuál implementamos variables, grupo de controles en un formulario, métodos,
ciclos de vida, etc, los cuales nos facilitarán el envio de la información a la vista */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
// pimeng
import {ConfirmationService} from 'primeng/api';
// models
import { Response, ResponseValidacion } from '../../models/datos.models';
import { Interes } from 'src/app/models/interes.models';
// services
import { AuthService } from '../../service/Auth/auth.service';
import { InteresService } from 'src/app/service/interes.service';
import { MensajesSweerAlertService } from '../../service/mensajes-sweer-alert.service';

@Component({
  selector: 'app-interes-crud',
  templateUrl: './interes-crud.component.html',
  styleUrls: ['./interes-crud.component.css']
})

export class InteresCrudComponent implements OnInit {

  listaInteres: Interes [] = [];
  interesSeleccionado!: Interes;
  paginador!: any;
  num = 0;
  // para poder cerrar subscripciones
  urlSubscription!: Subscription;
  urlSubscriptionBus!: Subscription;
  terminoBusqueda: string = '';
  // verificar campos para busqueda
  verificarTermino: boolean = false;

  // mensajes de error
  mensajeError!: string;

  // Constructor con inyección de dependencias
  constructor(
    private confirmationService: ConfirmationService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    public authService: AuthService,
    private interesService: InteresService,
    private mensajesService: MensajesSweerAlertService
  ){
  }
  // Ciclos de vida ngOnInit
  ngOnInit(): void {
     this.listaInteresPag();
  }
  // Ciclos de vida OnDestroy me permite cerrar los subcribes al destruir el componente
  ngOnDestroy(): void {
    this.cerrarSubscripcionLista();
    this.cerrarSubscripcionUrlBus();
  }

  // Método para cambiar de paginacion
  listaInteresPag = (): void => {
    // si existe otra supscripcion a params cerramos
    this.cerrarSubscripcionUrlBus();
    this.urlSubscription = this.activatedRouter.paramMap.subscribe( params => {
      // leemos y validamos el parametro url
      // @ts-ignore
      let pagina = +params.get('page');
      if ( !pagina) {
        pagina = 0;
      }
      this.num = pagina;
      // metodo del servicio para listar
      this.interesService.listaInteres(pagina)
        .subscribe( (response: any) => {
            if (response.empty) {
              this.listaInteres = [];
              this.paginador = null;
              // Me redirecciona a la ventana interes-crud
              this.router.navigateByUrl('interes/:page');
              this.cerrarSubscripcionLista();
              return;
            }
            // el dato emitido va a ser de tipo Interes
            this.listaInteres = (response.content as Interes[]);
            this.paginador = response;
        }, error => {
          this.cerrarSubscripcionLista();
          if (error.status === 0 ) this.mensajeError = 'Error de conección con el servidor';
        }
      );
    });
  }

  // Método para eliminar un interes mediante un id
  eliminarInteres = (interes: Interes) => {
    this.cerrarSubscripcionLista();
    this.cerrarSubscripcionUrlBus();
    // para el confirm dialog de primeng
    this.confirmationService.confirm({
        header: '¿Confirmar esta acción?',
        message: `Desea eliminar el Interes : ${interes.nombre}`,
        icon: 'pi pi-info-circle',
        acceptLabel: 'Si',
        accept: () => {
           if( interes ) {
              // eliminamos seguimiento
              this.interesService.borrarInteres(interes.id)
                  .subscribe( (response: Response) => {
                    this.mensajesService.mensajeSweetInformacionToast('success', response.mensaje);
                    // pasos para ver si existe mas de un dato si ahy una busqueda
                    if ( this.terminoBusqueda.length > 0 && this.paginador.totalPages >= 1 && this.paginador.totalElements > 1 ) {
                      this.filtrarInteres();
                      return;
                    }
                    // si el array contiene un 1 se regresa al inicio
                    if ( this.paginador.totalPages === 1 && this.paginador.totalElements === 1 ) {
                      this.terminoBusqueda = '';
                      this.listaInteresPag();
                      return;
                    }
                    this.listaInteresPag();
                  }, error => {
                    const errores = error.error as ResponseValidacion;
                    this.mensajesService.mensajeSweetFire('error', errores.error,errores.mensaje);
                    this.terminoBusqueda = '';
                    this.listaInteresPag();
                  });
           }
        }
    });
  }
  // Método para enrutar al inicio del paginador
  verificarSeleccionCampo = (): void => {
    this.router.navigateByUrl('interes/0');
  }


  // Método para filtrar la busqued
  filtrarInteres = ( ): void => {
    let termino = this.terminoBusqueda.trim();
    // si existe otra supscripcion a params cerramos
    this.cerrarSubscripcionLista();
    this.cerrarSubscripcionUrlBus();
    // validamos campos de busqueda
    if ( termino.length === 0) {
      this.listaInteresPag();
      this.verificarTermino = true;
      return;
    }
    this.verificarTermino = false;
    // busqueda para filtrar por campo
    this.urlSubscriptionBus = this.activatedRouter.paramMap.subscribe( params => {
      // leemos y validamos el parametro url
      // @ts-ignore
      let pagina = +params.get('page');
      if ( !pagina || this.listaInteres.length <= 1 ) {
        pagina = 0;
      }
      // método del service para buscar intereses
      this.interesService.listarInteresBusqueda( pagina, termino)
          .subscribe( (response: Response) => {
            this.listaInteres = (response.respuesta.content as Interes[]);
            this.paginador = response.respuesta;
          }, error => {
            this.mensajesService.mensajeSweetInformacion('info', `No  se encontro registros con : "${this.terminoBusqueda}"`);
            this.terminoBusqueda = '';
            this.listaInteresPag();
          });
    });
  }

  // Método para cerrar la Subscripción
  cerrarSubscripcionLista = (): void => {
    if ( this.urlSubscription ) this.urlSubscription.unsubscribe();
  }

  // Método para cerrar la Subscripción
  cerrarSubscripcionUrlBus = (): void => {
    if ( this.urlSubscriptionBus ) this.urlSubscriptionBus.unsubscribe();
  }
}
