/* El archivo typesCript es el lugar donde se maneja la lógica del componente tarjeta,
en la cuál implementamos variables, grupo de controles en un formulario, métodos,
ciclos de vida, etc, los cuales nos facilitarán el envio de la información a la vista */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

// modelos
import { Tarjeta } from 'src/app/models/tarjeta.model';
import { Response } from '../../models/datos.models';
// services
import { TarjetaService } from '../../service/tarjeta.service';
import { MensajesSweerAlertService } from '../../service/mensajes-sweer-alert.service';

@Component({
  selector: 'app-agregar-tarjeta',
  templateUrl: './agregar-tarjeta.component.html',
  styleUrls: ['./agregar-tarjeta.component.css']
})
export class AgregarTarjetaComponent implements OnInit {
 
  erroesBack: string [] = [];
  formSubmitted = false;
  tarjetaForm!: FormGroup;
  // vendedorForm: FormGroup;
  tarjeta!: Tarjeta;
  tarjetaSeleccionado!: Tarjeta;

  // Constructor con inyección de dependencias
  constructor(
    private formBuider: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private tarjetaService: TarjetaService,
    private mensajesService: MensajesSweerAlertService
  ) {}

  // Ciclo de vida ngOnInit
  ngOnInit(): void {
    this.buscarTarjetaId();
    this.tarjetaForm = this.formBuider.group({
      codigo: ['', [Validators.required, Validators.minLength(2)]],
      fechaCaducar:  [ new Date(), [Validators.required]]
    });
  }

  // Método donde verificamos si esta de actualizar o agregar tarjetas
  enviarDatos = (): void => {
    this.formSubmitted = true;
    this.erroesBack = [];
    // Desestructuración De Objetos
    const {codigo, fechaCaducar} = this.tarjetaForm.value;
    this.tarjeta = {id: 0, fechaCaducar, fechaGrabado: new Date(), activo: true, codigo};
    if ( this.tarjetaSeleccionado && this.tarjetaForm.valid ) {
      this.actualizarTarjeta(this.tarjetaSeleccionado.id);
    } else if (this.tarjetaForm.valid) {
      this.crearTarjeta(this.tarjeta);
    }
  }

  // Método para resetar campos del formulario
 limpiarCamposFormulario = (): void => {
    this.tarjetaForm.reset();
    this.tarjetaForm.get('fechaCaducar')?.setValue(new Date());
    this.formSubmitted = false;
  }

  // Método para crear Tarjeta
  crearTarjeta = (tarjeta: Tarjeta) => {
      this.tarjetaService.crearTarjeta(tarjeta)
        .subscribe( response => {
            let data = response as Response;
            // mensaje
            this.mensajesService.mensajeSweetInformacion('success', data.mensaje);
            this.limpiarCamposFormulario();
          }, error => this.erroresBackEnd(error));
  }

// Método donde actualizamos Tarjeta
  actualizarTarjeta = (id: number): void => {
    this.tarjetaService.actualizarTarjeta(id, this.tarjeta)
          .subscribe( response => {
            let data = response as Response;
            // mensaje
            this.mensajesService.mensajeSweetInformacion('success', data.mensaje);
            this.limpiarCamposFormulario();
            this.router.navigateByUrl('tarjeta/:page');
          }, error => this.erroresBackEnd(error));
  }

  // Método para buscar una tarjeta mediante su id
  buscarTarjetaId = (): void => {
    this.activatedRoute.paramMap
        .subscribe( params => {
            // @ts-ignore
            let id = +params.get('id');
            if ( id ) {
              this.tarjetaService.buscarPorIdTarjeta(id)
                  .subscribe( (response) => {
                    // el dato emitido va a ser de tipo Cliente
                    this.tarjetaSeleccionado = (response.respuesta as Tarjeta);
                    let {codigo, fechaCaducar } = this.tarjetaSeleccionado;
                    fechaCaducar = new Date(fechaCaducar);          
                    this.tarjetaForm.setValue({codigo, fechaCaducar });
                  }, error => {
                    // si da error regresamos a la tabla
                    this.mensajesService.mensajeSweetInformacion('warning', error.error.mensaje)
                    this.router.navigateByUrl('tarjeta/:page');
                  });
            } else {
              return;
            }
        });
  }

  // Método para guardar los posibles erroes del back
  erroresBackEnd = (error: any): void => {
    if (error.errores) {
      this.erroesBack = error.errores;
    } else {
      this.erroesBack.push(error.mensaje);
    }
  }
}
