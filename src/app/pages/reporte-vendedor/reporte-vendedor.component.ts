import { Component, OnInit } from '@angular/core';

// modelos
import { GenerarReporte, GenerarReporteXlsx } from '../../models/reportes/generar-reporte-model';
import { Response } from '../../models/datos.models';
import { Vendedor } from '../../models/vendedor.model';
import { VendedorReporte } from '../../models/reportes/vendedor-reporte.models';

// sevicios
import { AuthService } from '../../service/Auth/auth.service';
import { MensajesSweerAlertService } from '../../service/mensajes-sweer-alert.service';
import { ReportePdfService } from '../../service/reportes/reporte-pdf.service';
import { ReporteXlsxService } from '../../service/reportes/reporte-xlsx.service';
import { VendedorService } from '../../service/vendedor.service';

@Component({
  selector: 'app-reporte-vendedor',
  templateUrl: './reporte-vendedor.component.html',
  styleUrls: ['./reporte-vendedor.component.css']
})
/**
 * Generar vista de datos de vendedore y reporte
 */
export class ReporteVendedorComponent implements OnInit {

  vendedores: Vendedor [] = [];
  datosVendedoresReporte: VendedorReporte [] = [];
  columnas: any = [];
  exportarColumnas: any = [];
  // para el reporte
  cabeceras: string [] = [];
  termino: string = '';
  constructor(
    private authService: AuthService,
    private mensajeService: MensajesSweerAlertService,
    private reportePdfService: ReportePdfService,
    private reporteXlsxService: ReporteXlsxService,
    private vendedorService: VendedorService
  ) { }

  ngOnInit(): void {
    // cargamos cabeceras en la tabla
    this.cargarColumnasTabla();
    // cargamos datos vendedores
    this.listaVendedores();
  }
  
  cargarColumnasTabla = (): void => {
    this.columnas = [
      { field: 'vendedor',    header: 'Vendedor' },
      { field: 'clientes',    header: 'Cant. Clientes' },
      { field: 'clientesAct', header: 'Clientes Activo' },
      { field: 'clientesDes', header: 'Clientes Inactivos' },
      { field: 'promNivel',   header: 'Promedio Satisfacción' },
    ];
    this.exportarColumnas = this.columnas.map((col:any) => {
      this.cabeceras.push(col.header);
      return {      
      title: col.header,
      dataKey: col.field
    }});
  }

  // verificamos si hay un valor a buscar por el termino
  listaVendedores = (): void => {
    this.datosVendedoresReporte = [];
    if ( this.termino ){
      this.listaVendedoresClientesNombres();
    } else {
      this.listaVendedoresClientes();
    }
  }

  // lsita de todos los vendedores con clientes
  listaVendedoresClientes = (): void => {
    this.vendedorService.listaVendedoresClientes()
                        .subscribe ( (response: Response) => {
                          this.vendedores = response.respuesta as Vendedor [];
                          this.cargarDatosGenerarReporte(this.vendedores);
                        }, error => {
                          if ( this.datosVendedoresReporte.length === 0) this.mensajeService.mensajeSweetInformacionToast('info', 'No hay datos');
                        })
  }
  // lsita de todos los vendedores con clientes y con el el termino filtrado
  listaVendedoresClientesNombres = (): void => {
    this.vendedorService.listaVendedoresClientesNombres(this.termino)
                        .subscribe ( (response: Response) => {
                          this.vendedores = response.respuesta as Vendedor [];
                          console.log( this.vendedores);
                          this.cargarDatosGenerarReporte(this.vendedores);
                        }, error => {
                          if ( this.datosVendedoresReporte.length === 0) this.mensajeService.mensajeSweetInformacionToast('info', 'No hay datos');
                        })
  }

  // generamos datos para el reporte
  cargarDatosGenerarReporte = (vendedores: Vendedor []): void => {
    // creamos un objeto
    let vendedorReporte: VendedorReporte;
    // recorremos datos de vendedores
    for (const vendedor of vendedores) {
      // variables para calcular lo requerido
      let nombre =  `${vendedor.nombres} ${vendedor.apellidos}`
      let promNivel: string |number = 0;
      let clientesAct = 0;
      let clientesDes = 0;
      let clientes = 0;
      // si un vendedor tiene clientes  
      if ( vendedor.clientes) {
        // catidad de clientes
        clientes = vendedor.clientes.length;
        // para el numero de seguiminetos 
        let divisor = 0;
        // suma total de valor de un seguimiento 
        let suma = 0;
        // recorremos cada cliente de un vendedor
        for (const cliente of vendedor.clientes) {
          // para saber cuantos activo e inactivos
          cliente.eliminado ? clientesAct++ : clientesDes++;
          // recorremos cada seguiminto de un cliente si este es valido          
          if (cliente.seguimientos.length > 0 && cliente.eliminado){            
            for (const seguimiento of cliente.seguimientos) {
              divisor++;
              // acumulamos la suma 
              suma +=seguimiento.nivelSatisfaccion;
            }
          }
        }
        // dividimos para sacar el promedio
        if (divisor > 0){
          promNivel = (suma/divisor).toFixed(2);
        } else {
          promNivel = 'Sin seguimientos';
        }
      }
      // asignamos los datos
      vendedorReporte = {vendedor: nombre, clientes, clientesAct, clientesDes,  promNivel};
      // agregamos al arrary principal
      this.datosVendedoresReporte.push(vendedorReporte);
    }    
  }

  // generamos pdf
  generarPdf = (): void => {
    // si no hay datos a verifivar
    if ( this.datosVendedoresReporte.length === 0 ) {
      this.mensajeService.mensajeSweetInformacionToast('info', 'No hay datos a exportar')
      return;
    }
    // objeto para pasar los datos a generar el reporte
    let modeloReporte: GenerarReporte = {
                                          autor:  this.authService.usuario.name,
                                          titulo: 'Reporte Vendedor',
                                          parametros: 'Reporte de vendedores sobre la satisfación  de sus clientes',
                                          nombreArch:'vendedor',
                                          columnas: this.exportarColumnas,
                                          datos: this.datosVendedoresReporte
                                        }
    // pasamos el objeto  
    this.reportePdfService.exportPdf(modeloReporte, 80, 20);
  }
  generarXlxs = (): void => {
    if ( this.datosVendedoresReporte.length === 0 ) {
      this.mensajeService.mensajeSweetInformacionToast('info', 'No hay datos a exportar')
      return;
    }
    // objeto para pasar los datos a generar el reporte
    let modeloReporte: GenerarReporteXlsx = {
                                              autor: this.authService.usuario.name,
                                              nombreArch: 'vendedor',
                                              titulo: 'Reporte Vendedores',
                                              cabeceras: this.cabeceras,
                                              datos: this.datosVendedoresReporte
                                            }
    // pasamos el objeto  
    this.reporteXlsxService.exportarXlsx(modeloReporte);
  }
}
