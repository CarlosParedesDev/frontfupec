/* El archivo typesCript es el lugar donde se maneja la lógica del componente tarjeta-crud,
en la cuál implementamos variables, grupo de controles en un formulario, métodos,
ciclos de vida, etc, los cuales nos facilitarán el envio de la información a la vista */
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
// Rutas
import { ActivatedRoute, Router } from '@angular/router';
// primeng
import {ConfirmationService} from 'primeng/api';
// modelos
import { Tarjeta } from '../../models/tarjeta.model';
import { Response, ResponseValidacion } from '../../models/datos.models';
// services
import { AuthService } from '../../service/Auth/auth.service';
import { MensajesSweerAlertService } from 'src/app/service/mensajes-sweer-alert.service';
import { TarjetaService } from '../../service/tarjeta.service';


@Component({
  selector: 'app-tarjeta-crud',
  templateUrl: './tarjeta-crud.component.html',
  styleUrls: ['./tarjeta-crud.component.css']
})

export class TarjetaCrudComponent implements OnInit, OnDestroy {
  num = 0;
  terminoBusqueda = '';
  verificarTermino = false;

  // para poder cerrar subscripciones
  urlSubscription!: Subscription;
  urlSubscriptionBus!: Subscription;

  listaTarjetas: Tarjeta [] = [];
  paginadorTarjeta!: any;

  // mensajes de error
  mensajeError!: string;

  // Constructor con inyección de dependencias
  constructor(
    private confirmationService: ConfirmationService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    public  authService: AuthService,
    private tarjetaService: TarjetaService,
    private mensajesService: MensajesSweerAlertService
  ){
  }

  // Ciclos de vida ngOnInit
  ngOnInit(): void {
    this.listaTarjetaPag();
  }
  // Ciclos de vida ngOnDestroy
  ngOnDestroy(): void {
    this.cerrarSubscripcionLista();
    this.cerrarSubscripcionUrlBus();
  }

  // Método para cambiar de paginacion
  listaTarjetaPag = (): void => {
    // cerrar el subscribe de filtrarTarjeta
    this.cerrarSubscripcionUrlBus();
    this.urlSubscription = this.activatedRouter.paramMap.subscribe( params => {
      // leemos y validamos el parametro url
      //@ts-ignore
      let pagina = +params.get('page');
      if ( !pagina ) {
        pagina = 0;
      } 
      this.num = pagina;
      // metodo del servicio para listar
      this.tarjetaService.listaTarjeta(pagina)
        .subscribe( (response: any) => {
          if (response.empty) {
            this.listaTarjetas = [];
            this.paginadorTarjeta = null;
            // cambiar la ruta de navegación
            this.router.navigateByUrl('tarjeta/:page');
            this.cerrarSubscripcionLista();
            return;
          }
          // el dato emitido va a ser de tipo tarjeta
          this.listaTarjetas = (response.content as Tarjeta[]);
          this.paginadorTarjeta = response;
        }, error => {
        // mensaje
        this.cerrarSubscripcionLista();
        if (error.status === 0 ) this.mensajeError = 'Error de conección con el servidor';
        }
      );
    });
  }

  // Método para eliminar una tarjeta mediante un id
  eliminarTarjeta = (tarjeta: Tarjeta) => {
    this.cerrarSubscripcionLista();
    this.cerrarSubscripcionUrlBus();
    // para el confirm dialog de primeng
    this.confirmationService.confirm({
        header: '¿Confirmar esta acción?',
        message: `Desea eliminar la tarjeta con el Código: ${tarjeta.codigo}`,
        icon: 'pi pi-info-circle',
        acceptLabel: 'Si',
        accept: () => {
            if( tarjeta ) {
              // eliminamos tarjeta
              this.tarjetaService.borrarTarjeta(tarjeta.id)
                  .subscribe( (response: Response) => {
                    this.mensajesService.mensajeSweetInformacionToast('success', response.mensaje);
                    // pasos para ver si existe mas de un dato si ahy una busqueda
                    if ( this.terminoBusqueda.length > 0 && this.paginadorTarjeta.totalPages >= 1 && this.paginadorTarjeta.totalElements > 1 ) { 
                      this.filtrarTarjetas();
                      return;
                    }
                    // si el array contiene un 1 se regresa al inicio
                    if ( this.paginadorTarjeta.totalPages === 1 && this.paginadorTarjeta.totalElements === 1 ) {
                      this.terminoBusqueda = '';
                      this.listaTarjetaPag();
                      return;
                    }
                    this.listaTarjetaPag();
                  }, error => {
                    const errores = error.error as ResponseValidacion;
                    this.mensajesService.mensajeSweetFire('error', errores.error,errores.mensaje);
                    this.terminoBusqueda = '';
                    this.listaTarjetaPag();
                  }
                );
            }
        }
    });
  }
  // Método para enrutar al inicio del paginador
  cambiarUrl = () => this.router.navigateByUrl('tarjeta/0');

  // Método para filtrar la busqueda
  filtrarTarjetas = (): void => {
    let termino = this.terminoBusqueda.trim()
    // cerramos subscribes
    this.cerrarSubscripcionLista();
    this.cerrarSubscripcionUrlBus();
    // validamos campos de busqueda
    if ( termino.length === 0) {
      this.terminoBusqueda = '';
      this.verificarTermino = true;
      this.listaTarjetaPag();
      return;
    } 
    this.verificarTermino = false;
    // busqueda para filtrar por campo
    this.urlSubscriptionBus = this.activatedRouter.paramMap.subscribe( params => {
      // leemos y validamos el parametro url 
      //@ts-ignore
      let pagina = +params.get('page');
      if ( !pagina || this.listaTarjetas.length <= 1) {
        pagina = 0;
      }
      // método del servicio para buscar
      this.tarjetaService.listarTarjetaBusqueda(pagina, this.terminoBusqueda)
          .subscribe( (response: Response) => {
            // el dato emitido va a ser de tipo tarjeta
            this.listaTarjetas = (response.respuesta.content as Tarjeta[]);
            this.paginadorTarjeta = response.respuesta;
          }, error => {
            // mensaje
            this.mensajesService.mensajeSweetInformacion('info', `No hay tarjetas con este: "${this.terminoBusqueda}"`)
            this.terminoBusqueda = '';
            this.listaTarjetaPag();
          });
    });
    this.cerrarSubscripcionLista();
  }

  // Método para cerrar la Subscripción
  cerrarSubscripcionLista = (): void => {
    if ( this.urlSubscription ) this.urlSubscription.unsubscribe();
  }

  // Método para cerrar la Subscripción
  cerrarSubscripcionUrlBus = (): void => {
    if ( this.urlSubscriptionBus ) this.urlSubscriptionBus.unsubscribe();
  }
}
