/* El archivo typesCript es el lugar donde se maneja la lógica del componente login,
en la cuál implementamos variables, grupo de controles en un formulario, métodos,
ciclos de vida, etc, los cuales nos facilitarán el envio de la información a la vista */
import { Component, OnInit } from '@angular/core';
// Formulario reactivo
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// Rutas
import { ActivatedRoute, Router } from '@angular/router';
// modelos
import { Cliente } from '../../models/cliente.models';
import { Interes } from '../../models/interes.models';
import { Response } from '../../models/datos.models';
import { Tarjeta } from 'src/app/models/tarjeta.model';
import { Vendedor } from 'src/app/models/vendedor.model';
import { User } from '../../models/user.model';
import { User_groups } from '../../models/user_groups.model';
import { Usuario } from 'src/app/models/usuario.models';

// servicios
import { AuthService } from '../../service/Auth/auth.service';
import { ClienteService } from '../../service/cliente.service';
import { InteresService } from '../../service/interes.service';
import { MensajesSweerAlertService } from '../../service/mensajes-sweer-alert.service';
import { TarjetaService } from '../../service/tarjeta.service';
import { UsuarioClienteService } from '../../service/usuario-cliente.service';
import { VendedorService } from '../../service/vendedor.service';

@Component({
  selector: 'app-agregar-cliente',
  templateUrl: './agregar-cliente.component.html',
  styleUrls: ['./agregar-cliente.component.css']
})
export class AgregarClienteComponent implements OnInit {

  erroesBack: string[] = [];
  formSubmitted = false; // para ver si se ha realizado alguna accion en el form
  clienteForm!: FormGroup;
  cliente!: Cliente;
  intereses: Interes [] = [];
  interesesActuales: Interes [] = [];
  clienteSeleccionado!: Cliente;
  usuarioCliente!:Usuario;
  usuarioClienteSeleccionado!:Usuario;


  // < ------------- para las filtraciones de interes, vendedor y tarjeta ----------->
  listaIntereses: Interes [] = [];
  listaTarjetas: Tarjeta [] = [];
  listaVendedores: Vendedor [] = [];
  // Mensajes de erros
  mensajeErrorTarjeta!: string;
  mensajeErrorVendedor!: string;
  mensajeError!: string;
  // <------------------ lista generos ----------------------->
  generos = [
    {label: 'Hombre', value: 'Hombre'},
    {label: 'Mujer', value: 'Mujer'},
    {label: 'Otro', value: 'Otro'}
    ];
  // <------------------ lista tipo sangre ----------------------->
  tiposSangre = [
    {label: 'A+', value: 'A+'},
    {label: 'A-', value: 'A-'},
    {label: 'B+', value: 'B+'},
    {label: 'B-', value: 'B-'},
    {label: 'AB+', value: 'AB+'},
    {label: 'AB-', value: 'AB-'},
    {label: 'O+', value: 'O+'},
    {label: 'O-', value: 'O-'},
    ];

  // Constructor con inyección de dependencias
  constructor(
    private formBuider: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private clienteService: ClienteService,
    private interesService: InteresService,
    private mensajesService: MensajesSweerAlertService,
    private tarjetaService: TarjetaService,
    private usuarioService: UsuarioClienteService,
    private vendedorService: VendedorService,
  ) { }

  // Ciclos de vida ngOnInit
  ngOnInit(): void {
    // buscamos id por la url
    this.buscarClienteId();
    // para el select multiple
    this.listarIntereses();
    // Formulario reactivo
    this.crearFormularioCliente();
  }

  // Método donde tengo el FormGroup con sus validaciones
  crearFormularioCliente = (): void => {
    // formBuider me permite construir un formulario creando un grupo de controles
    this.clienteForm = this.formBuider.group({
      nombre: ['', [Validators.required, Validators.minLength(2)]],
      usuario: [ '', [Validators.required, Validators.email]],
      clave: [ '', [Validators.required, Validators.minLength(6)]],
      cedula: [ '', [Validators.required, Validators.pattern(/^[0-9]\d{9}$/) ]],
      telefono: [ '', [Validators.required, Validators.pattern(/^[0-9]\d{6,10}$/) ]],
      genero: [ '', [Validators.required]],
      ocupacion: [ '', [Validators.required, Validators.minLength(2)]],
      tipoSangre: [ '', [Validators.required, Validators.minLength(1)]],
      puntos: [ 0, [Validators.required]],
      tarjeta: ['', [Validators.required]],
      vendedor: ['', [Validators.required]],
      intereses: ['',[]]
    });
  }

  // Método donde verificamos si hay parametro id valido por url
  buscarClienteId = (): void => {
    // extraemos datos de la url
    this.activatedRoute.paramMap
      .subscribe(params => {
        // @ts-ignore
        let id = +params.get('id');
        if (id) {
          // buscamos el vendedor por id
          this.clienteService.buscarPorIdCliente(id)
            .subscribe((response) => {
              // el dato emitido va a ser de tipo Cliente
              this.clienteSeleccionado = response.respuesta as Cliente;
              // Desestructuración De Objetos
              let {cedula, genero, ocupacion, tipoSangre, puntos, tarjeta, vendedor, intereses } = this.clienteSeleccionado;
              this.usuarioClienteSeleccionado = this.clienteSeleccionado.usuario;
              let {nombre , usuario, telefono, clave} = this.clienteSeleccionado.usuario;
              // mostramos datos del cliente seleccionado
              // tslint:disable-next-line:max-line-length
              this.clienteForm.setValue({ nombre, cedula, telefono, usuario, genero, ocupacion, tipoSangre, puntos,tarjeta, vendedor, clave, intereses });
            }, error => {
              // si da error regresamos a la tabla
              this.mensajesService.mensajeSweetInformacion('warning', error.error.mensaje);
              this.router.navigateByUrl('cliente/:page');
            });
        } else {
          return;
        }
      });
  }

  // verifica si esta de actualizar o agregar vendedor
  enviarDatos = (): void => {
    this.mensajeError = '';
    this.formSubmitted = true;
    this.erroesBack = [];
    // desestructuramos datos del formulario
    let { usuario, clave, nombre, telefono, cedula, genero, ocupacion, puntos, tarjeta, vendedor, tipoSangre, intereses } = this.clienteForm.value;
    let validarIntereses = intereses || [];
    this.usuarioCliente = {id:0, nombre, clave, usuario, telefono}; // para el usuariocliente
    // para enviar el usuario autor
    let {id, name, username, password} = this.authService.usuario;
    let userLevel: User_groups = {id:0, groupName:''}
    let user: User = {id, name, username, userLevel, password};
    this.cliente = {  id:0, cedula, 
                      genero, ocupacion, 
                      tipoSangre, puntos, 
                      tarjeta, vendedor, 
                      eliminado: true, usuario, 
                      seguimientos: [], intereses: validarIntereses,
                      user
                    };
    // para validar si son objetos
    if (typeof(tarjeta) === 'string') {
      this.mensajeErrorTarjeta = 'Seleccione correctamente este campo';
      return;
    } else if ( typeof(vendedor) === 'string' ) {
      this.mensajeErrorVendedor = 'Seleccione correctamente este campo';
      return;
    }
    //  validamos que los puntos no sean negativos
    if (this.cliente.puntos < 0) {
      this.mensajeError = 'Puntos debe ser igual o mayor a 0';
      return;
    }
    if ( this.clienteSeleccionado && this.usuarioClienteSeleccionado && this.clienteForm.valid ) { // verificamos si  hay datos a actualizar
      this.actualizarUsuarioCliente(this.usuarioClienteSeleccionado.id); // pasamos id parametro
    } else if (this.clienteForm.valid) { // caso contrario creamos vendedor
      this.crearUsuariocliente();
    }
  }

  // Método para crear Usuarios
  crearUsuariocliente = (): void => {
    this.usuarioService.crearUsuarioCliente( this.usuarioCliente )
        .subscribe(response => {
          let data = response as Response;
          // mandamos objeto usuario a al cliente
          this.cliente.usuario = data.respuesta as Usuario;
          // Guardamos cliente
          this.crearCliente();
        }, error => this.erroresBackEnd(error));
  }

  // Método para crear Cliente
  crearCliente = (): void => {
    this.clienteService.crearCliente(this.cliente)
      .subscribe(response => {
        let data = response as Response;
        this.mensajesService.mensajeSweetInformacion('success', data.mensaje);
        this.limpiarCamposFormulario();
      }, error => {
        this.eliminarUsuarioCliente();
        this.erroresBackEnd(error);
      });
  }

  // Método donde actualizamos usuario de la tabla usuarios
  actualizarUsuarioCliente = (id: number): void => {
    this.usuarioService.actualizarUsuarioCliente(id, this.usuarioCliente)
        .subscribe( (response: Response) => {
          // mandamos objeto usuario a al cliente
          this.cliente.usuario = response.respuesta as Usuario;
          this.actualizarCliente(this.clienteSeleccionado.id);
        }, error => this.erroresBackEnd(error));
  }

  // Método donde actualizamos Cliente
  actualizarCliente = (id: number): void => {
    this.clienteService.actualizarCliente(id, this.cliente)
      .subscribe((response: Response) => {
        this.mensajesService.mensajeSweetInformacion('success', response.mensaje);
        this.limpiarCamposFormulario();
        this.router.navigateByUrl('cliente/:page');
      }, error => this.erroresBackEnd(error));
  }

  // Método donde eliminamos usuario de la tabla usuarios si no se gurda correctamente el cliente
  eliminarUsuarioCliente = (): void => {
    this.usuarioService.borrarUsuarioCliente(this.cliente.usuario.id).subscribe();
  }

  // Método para guardar los posibles erroes del back
  erroresBackEnd = (error: any): void => {
    if (error.status === 400) this.erroesBack.push('Hay campos vacios');
    if (error.errores) {
      this.erroesBack = error.errores;
    } else {
      this.erroesBack.push(error.mensaje);
    }
  }

  // Método para resetar campos del formulario
  limpiarCamposFormulario = (): void => {
    this.clienteForm.reset();
    this.formSubmitted = false;
  }

  //  Metodos para las listas de vendedor, tarjetas por termino
  listarDatos = (event: any, tipo: string): void => {
    let termino = event.query.trim();
    if (termino.length == 0){
      this.listaTarjetas = [];
      this.listaVendedores = [];
      termino = '';
      return;
    }
    // verificamos el tipo de filtrado
    tipo === 'tarjeta' ? this.listaTarjetasCodigo(termino) : this.listaVendedoresEmail(termino);
  }

  // Método donde listamos tarjetas en el input de filtrado
  listaTarjetasCodigo = (termino: string): void => {
    this.tarjetaService.buscarPorCodigoTarjeta( termino )
        .subscribe( (response: Response) => {
          this.listaTarjetas = response.respuesta as Tarjeta[];
          this.mensajeErrorTarjeta = '';
        },
        error => {
          this.listaTarjetas = [];
          this.mensajeErrorTarjeta = error.error.error;
        });
  }

  // Método donde listamos vendedores en el input de filtrado
  listaVendedoresEmail = (termino: string): void => {
    this.vendedorService.listaVendedoresEmail(termino)
        .subscribe( (response: Response) => {
          this.listaVendedores = response.respuesta as Vendedor[];
          this.mensajeErrorVendedor = '';
        },
        error => {
          this.listaVendedores = [];
          this.mensajeErrorVendedor = error.error.error;
        });
  }

  // Método donde listamos los intereses
  listarIntereses = (): void => {
    this.interesService.listarIntereses()
        .subscribe( (response: Response) => {
          this.listaIntereses = response.respuesta as Interes[];
        }, error => this.listaIntereses = []);
  }

}
