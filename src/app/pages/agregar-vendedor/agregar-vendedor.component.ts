/* El archivo typesCript es el lugar donde se maneja la lógica del componente vendedor,
en la cuál implementamos variables, grupo de controles en un formulario, métodos,
ciclos de vida, etc, los cuales nos facilitarán el envio de la información a la vista */
import { Component, OnInit } from '@angular/core';
// Formulario reactivo
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// Rutas
import { ActivatedRoute, Router } from '@angular/router';
// modelos
import { Vendedor } from '../../models/vendedor.model';
import { Response } from '../../models/datos.models';
// servicios
import { VendedorService } from '../../service/vendedor.service';
import { MensajesSweerAlertService } from '../../service/mensajes-sweer-alert.service';

@Component({
  selector: 'app-agregar-vendedor',
  templateUrl: './agregar-vendedor.component.html',
  styleUrls: ['./agregar-vendedor.component.css']
})
export class AgregarVendedorComponent implements OnInit {

  erroesBack: string [] = [];
  formSubmitted = false; // para ver si se ha realizado alguna accion en el form
  vendedorForm!: FormGroup;
  vendedor!: Vendedor;
  vendedorSeleccionado!: Vendedor;

  // Constructor con inyección de dependencias
  constructor(
    private formBuider: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private vendedorService: VendedorService,
    private mensajesService: MensajesSweerAlertService
  ) {}

  // Ciclo de vida ngOnInit
  ngOnInit(): void {
    // buscamos id por la url
    this.buscarVendedorId();
    // formBuider me permite construir un formulario creando un grupo de controles
    this.vendedorForm = this.formBuider.group({
      nombres:   [ '', [Validators.required, Validators.minLength(2)]],
      apellidos: [ '', [Validators.required, Validators.minLength(2)]],
      email:     [ '', [Validators.required, Validators.email]],
      telefono:  [ '', [Validators.required, Validators.pattern(/^[0-9]\d{6,10}$/) ]],
    });
  }

  // Método donde verifica si esta de actualizar o agregar vendedor
  enviarDatos = (): void => {
    this.formSubmitted = true;
    this.erroesBack = [];
    // desestructuramos los datos del formulario
    const {nombres, apellidos, email, telefono} = this.vendedorForm.value;
    // creamos objeto
    this.vendedor = {id:0, nombres, apellidos, email,telefono, fechaGrabado: new Date(), eliminado: true}; 
    if ( this.vendedorSeleccionado && this.vendedorForm.valid ) { // verificamos si  hay datos a actualizar
      this.actualizarVendedor(this.vendedorSeleccionado.id);
    } else if (this.vendedorForm.valid) { // caso contrario creamos vendedor
      this.crearVendedor();
    }
  }

  // Método para resetar campos del formulario
  limpiarCamposFormulario = (): void => {
    this.vendedorForm.reset();
    this.formSubmitted = false;
  }

  // Método para crear Vendedor
  crearVendedor = (): void => {
      this.vendedorService.crearVendedor(this.vendedor)
          .subscribe( response => {
            let data = response as Response;
            this.mensajesService.mensajeSweetInformacion('success', data.mensaje);
            this.limpiarCamposFormulario();
          }, error => this.erroresBackEnd(error));
  }

  // Método para actualizar Vendedor
  actualizarVendedor = (id: number): void => {
    this.vendedorService.actualizarVendedor(id, this.vendedor)
          .subscribe( response => {
            let data = response as Response;
            // mensaje
            this.mensajesService.mensajeSweetInformacion('success', data.mensaje);
            this.limpiarCamposFormulario();
            this.router.navigateByUrl('vendedor/:page');
          }, error => this.erroresBackEnd(error));
  }

  // Método para buscar vendedor por medio del id
  buscarVendedorId = (): void => {
    // extraemos datos de la url
    this.activatedRoute.paramMap
        .subscribe( params => {
            // @ts-ignore
            let id = +params.get('id');
            if ( id ) {
              // buscamos el vendedor por id
              this.vendedorService.buscarPorIdVendedor(id)
                  .subscribe( (response) => {
                    // el dato emitido va a ser de tipo Vendedor
                    this.vendedorSeleccionado = (response.respuesta as Vendedor);
                    const { nombres, apellidos, email, telefono } = this.vendedorSeleccionado;
                    // ingresamos los datos emitidos a los campos del formulario
                    this.vendedorForm.setValue({ nombres, apellidos, email, telefono });
                  }, error => {
                    // si da error regresamos a la tabla
                    this.mensajesService.mensajeSweetInformacion('warning', error.error.mensaje)
                    this.router.navigateByUrl('vendedor/:page');
                  });
            } else {
              return;
            }
        });
  }

  // Método para guardar los posibles erroes del back
  erroresBackEnd = (error: any): void => {
    if (error.status === 400) this.erroesBack.push('Hay campos vacios');
    if (error.errores) {
      this.erroesBack = error.errores;
    } else {
      this.erroesBack.push(error.mensaje);
    }
  }
}
