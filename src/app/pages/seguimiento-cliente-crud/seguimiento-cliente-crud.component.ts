/* El archivo typesCript es el lugar donde se maneja la lógica del componente segumiento-crud,
en la cuál implementamos variables, grupo de controles en un formulario, métodos,
ciclos de vida, etc, los cuales nos facilitarán el envio de la información a la vista */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
// pimeng
import { ConfirmationService } from 'primeng/api';
// models
import { Cliente } from '../../models/cliente.models';
import { Response } from '../../models/datos.models';
import { Seguimiento } from 'src/app/models/seguimiento.models';
// services
import { AuthService } from '../../service/Auth/auth.service';
import { ClienteService } from '../../service/cliente.service';
import { MensajesSweerAlertService } from '../../service/mensajes-sweer-alert.service';
import { SeguimientoService } from 'src/app/service/seguimiento.service';


@Component({
  selector: 'app-seguimiento-cliente-crud',
  templateUrl: './seguimiento-cliente-crud.component.html',
  styleUrls: ['./seguimiento-cliente-crud.component.css']
})

export class SeguimientoClienteCrudComponent implements OnInit {

  listaClientesSeguimientos: Cliente[] = [];
  seguimientosCliente!: Cliente;
  paginador!: any;
  num = 0;
  seguimiento!: Seguimiento;
  seguimientosPorFecha: Seguimiento [] = [];
  // para poder cerrar subscripciones
  urlSubscription!: Subscription;
  urlSubscriptionBus!: Subscription;
  urlSubscriptionBusquedaFecha!: Subscription;

  seleccionCampo!: string;
  terminoBusqueda: string = '';
  campoFechaDesde:Date = new Date() ;
  campoFechaHasta: Date = new Date();
  // verificar campos para busqueda
  verificarCampo: boolean = false;
  verificarTermino: boolean = false;

  // mensajes de error
  mensajeError!: string;
  mensajeErrorFecha!: string;
  buscarPor = [
    { label: 'Cedula',  value: 'cedula' },
    { label: 'Nombre',  value: 'nombre' },
    { label: 'Fecha',   value: 'fecha' }
  ];
  // modal bandera
  abrir = false;

  // Constructor con inyección de dependencias
  constructor(
    private activatedRouter: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private router: Router,
    public authService: AuthService,
    private clienteService: ClienteService,
    private mensajesService: MensajesSweerAlertService,
    private seguimientoService: SeguimientoService
  ) {
  }

  // Ciclos de vida ngOnInit
  ngOnInit(): void {    
    this.listaClientesSeguimientoPaginacion();
  }

  // Ciclos de vida ngOnDestroy
  ngOnDestroy(): void {
    // cerramos subscripciones abiertas
    this.cerrarSubscripciones();
  }

  // Método para asignar valores iniciales a las fechas
  inicioRangoFechas = (): void => {
    this.campoFechaHasta = new Date();
    this.campoFechaDesde = new Date(this.campoFechaHasta.getFullYear(), this.campoFechaHasta.getMonth(), 1);
  }

   // Método para cambiar de paginacion
  listaClientesSeguimientoPaginacion = (): void => {
    // si existe otra supscripcion a params cerramos
    this.cerrarSubscripciones();
    this.urlSubscription = this.activatedRouter.paramMap.subscribe(params => {
      // leemos y validamos el parametro url
      // @ts-ignore
      let pagina = +params.get('page');
      if (!pagina) {
        pagina = 0;
      }
      this.num = pagina;
      // llamamos el servicio para realizar la peticion con el servidor
      this.clienteService.listarClientesSeguimientos(pagina)
      .subscribe((response: any) => {
        // vaciamos variables de las busquedas
        this.vaciamosCampos();
        if (response.empty) {
          this.listaClientesSeguimientos = [];
          this.paginador = null;
          this.router.navigateByUrl('seguimientoCliente/:page');
          this.cerrarSubscripcionLista();
          return;
        }
        // el dato emitido va a ser de tipo Cliente
          this.listaClientesSeguimientos = (response.content as Cliente[]);
          this.paginador = response;
        }, error => {
          this.cerrarSubscripcionLista();
          if (error.status === 0) this.mensajeError = 'Error de conección con el servidor';
        }
        );
    });
  }

  // Método para eliminar un seguimiento mediante un id
  eliminarSeguimiento = (seguimientoCliente: Seguimiento) => {
    let nombre = seguimientoCliente?.cliente?.usuario?.nombre || this.seguimientosCliente?.usuario?.nombre;
    // cerramos subscripcione abiertas
    this.cerrarSubscripciones();
    // para el confirm dialog de primeng
    this.confirmationService.confirm({
      header: '¿Confirmar esta acción?',
      message: `Desea eliminar el seguimiento del cliente: ${nombre}`,
      icon: 'pi pi-info-circle',
      acceptLabel: 'Si',
      accept: () => {
        if (seguimientoCliente) {
          // llamamos el servicio para realizar la peticion con el servidor
          this.seguimientoService.borrarSeguimiento(seguimientoCliente.id)
            .subscribe((response: Response) => {
              this.mensajesService.mensajeSweetInformacionToast('success', response.mensaje);
                // verificamos si el campo es fecha o diferente
              if ( this.seleccionCampo === 'fecha' ) {
                  this.seguimientoEliminadoPorFiltradoFecha();
              } else {
                  this.seguimientoEliminadoFiltrado(seguimientoCliente);
              }
            }, error => this.mensajesService.mensajeSweetFire('error', error, 'Acción denegada'));
        }
      }
    });
  }

  // Método para eliminar un seguimiento filtrado
  seguimientoEliminadoFiltrado = (seguimientoCliente: Seguimiento) => {
    // eliminamos el seguimiento del arrray
    this.seguimientosCliente.seguimientos = this.seguimientosCliente.seguimientos.filter(
      (seguimiento: Seguimiento) => seguimiento.id !== seguimientoCliente.id
    );
    // pasos para ver si existe mas de un dato si ahy una busqueda
    if ( this.terminoBusqueda.length > 0 && this.paginador.totalPages >= 1 && this.paginador.totalElements > 1 ) {
      // si el cliente no tiene seguimientos cerramos modal y volvemos a llamar el filtrar
      if (this.seguimientosCliente.seguimientos.length === 0 && this.abrir){
        this.filtrarSeguimientos();
        this.cerrarModal();
      }
      return;
    }
    // si el array contiene un 1 se regresa al inicio
    if ( this.paginador.totalPages === 1 && this.paginador.totalElements === 1 && this.seguimientosCliente.seguimientos.length === 0) {
      this.terminoBusqueda = '';
      this.seleccionCampo = '';
      this.cerrarModal();
      this.listaClientesSeguimientoPaginacion();
      return;
    }
    // si ya no tiene seguimientos cerramos el modal y volvemos  a listar los clientes
    if (this.seguimientosCliente.seguimientos.length === 0) {
      this.cerrarModal();
      this.listaClientesSeguimientoPaginacion();
      return;
    }
  }

  // Método para eliminar un seguimiento por fecha
  seguimientoEliminadoPorFiltradoFecha = () => {
    // pasos para ver si existe mas de un dato si hay una busqueda
    if ( this.campoFechaDesde && this.campoFechaHasta && this.paginador.totalPages >= 1 && this.paginador.totalElements > 1 ) {
      this.filtrarSeguimientos();
      return;
    }
    // si el array contiene un 1 se regresa al inicio
    if ( this.paginador.totalPages === 1 && this.paginador.totalElements === 1 ) {
      this.vaciamosCamposFechas();
      this.listaClientesSeguimientoPaginacion();
      return;
    }
    this.listaClientesSeguimientoPaginacion();
  }

  // Método para enrutar al inicio del paginador
  verificarSeleccionCampo = (): void => {
    this.verificamosValidacionesCampos();
    this.cerrarSubscripciones();
    this.router.navigateByUrl('seguimientoCliente/0');
    this.listaClientesSeguimientoPaginacion();
  }

  // Método para reiniciar valores al cambiar el campo
  verificamosValidacionesCampos = (): void => {
    this.seguimientosPorFecha = [];
    this.verificarCampo = false;
    this.verificarTermino = false;
    this.mensajeErrorFecha = '';
  }

  // Método para filtrar seguimientos por cliente cedula-nombre-fechas
  filtrarSeguimientos = (): void => {
    let termino = this.terminoBusqueda.trim();
    // si existe otra supscripcion a params cerramos
    this.cerrarSubscripciones();
    // para filtrar por fecha
    if (this.seleccionCampo === 'fecha') {
      if ( this.campoFechaDesde && this.campoFechaHasta ) {
        // invierte los valores en el caso de ingresar una fecha mayor al segundo campo
        if ( this.campoFechaDesde.getTime() > this.campoFechaHasta.getTime()){
          let fechaBandera = this.campoFechaHasta;
          this.campoFechaHasta = this.campoFechaDesde;
          this.campoFechaDesde = fechaBandera;
        }
        // método del servicio para buscar por fecha
        this.busquedaPorFechaSeguimiento( this.campoFechaDesde, this.campoFechaHasta);
        return
      }
      this.mensajeErrorFecha = 'Debe seleccionar un rango de fechas';
      return
    }
    // validamos campos de busqueda
    if ( termino.length === 0) {
      this.listaClientesSeguimientoPaginacion();
      this.verificarTermino = true;
      return;
    } else if ( !this.seleccionCampo) {
      this.verificarTermino = false;
      this.verificarCampo = true;
      return;
    }
    this.verificarTermino = false;
    this.urlSubscriptionBus = this.activatedRouter
                                  .paramMap
                                  .subscribe( params => {
                                    // leemos y validamos el parametro url 
                                    //@ts-ignore
                                    let pagina = +params.get('page');
                                    if ( !pagina || this.listaClientesSeguimientos.length <= 1 ) {
                                      pagina = 0;
                                    }
                                    // método del servicio para listar seguimientos
                                    this.clienteService.listarClientesSeguiminetosBusqueda(this.seleccionCampo, pagina, termino)
                                        .subscribe( (response: Response) => {
                                          // el dato emitido va a ser de tipo Cliente
                                          this.listaClientesSeguimientos = (response.respuesta.content as Cliente[]);
                                          this.paginador = response.respuesta;
                                        }, error => {
                                          // mensaje
                                          this.mensajesService.mensajeSweetInformacion('info', `No hay ${this.seleccionCampo} con : "${this.terminoBusqueda}"`);
                                          this.vaciamosCamposFechas();
                                          this.listaClientesSeguimientoPaginacion();
                                        });
                                  });
  }
  // Método para filtrar seguimientos por fecha
  busquedaPorFechaSeguimiento = (fechaDesde: Date, fechaHasta:Date): void => {
    this.mensajeErrorFecha = '';
    this.cerrarSubscripciones();
    this.urlSubscriptionBusquedaFecha = this.activatedRouter
                                            .paramMap
                                            .subscribe( params => {
                                              // leemos y validamos el parametro url
                                              //@ts-ignore
                                              let pagina = +params.get('page');
                                              if ( !pagina ) {
                                                pagina = 0;
                                              }
                                               // método del servicio para buscar seguimientos
                                              this.seguimientoService.buscarSeguimientoPorFecha(this.seleccionCampo, pagina,fechaDesde, fechaHasta)
                                                                      .subscribe((response: Response) => {
                                                                        // el dato emitido va a ser de tipo Cliente
                                                                        this.seguimientosPorFecha = response.respuesta.content as Seguimiento [];
                                                                        this.paginador = response.respuesta;
                                                                      }, error => {
                                                                        // mensaje
                                                                        this.mensajesService.mensajeSweetInformacion('info', `No hay seguimientos con esas fechas`);
                                                                        this.vaciamosCamposFechas();
                                                                        this.listaClientesSeguimientoPaginacion();
                                                                      });
                                            });
  }
  // Método para vaciar campos cuando las fechas son invalidas
  vaciamosCamposFechas = (): void => {
    this.inicioRangoFechas();
    this.seleccionCampo = '';
    this.seguimientosPorFecha = [];
  }

  // Método para vaciar campos cuando no hay datos del back-end
  vaciamosCampos = (): void => {
    this.inicioRangoFechas();
    this.terminoBusqueda = '';
  }
  // <---------------- metodos para cerrar la subscribe -------------------------->
  cerrarSubscripcionLista = (): void => {
    if (this.urlSubscription) this.urlSubscription.unsubscribe();
  }
  // subscripcion de busqueda
  cerrarSubscripcionUrlFiltrado = (): void => {
    if (this.urlSubscriptionBus) this.urlSubscriptionBus.unsubscribe();
  }
   // Método para cerrar la Subscripción de busqueda por fecha
  cerrarSubscripcionUrlBusquedaFecha = (): void => {
    if (this.urlSubscriptionBusquedaFecha) this.urlSubscriptionBusquedaFecha.unsubscribe();
  }
   // Método para cerrar todas las Subscripciones
  cerrarSubscripciones =(): void => {
    this.cerrarSubscripcionLista();
    this.cerrarSubscripcionUrlFiltrado();
    this.cerrarSubscripcionUrlBusquedaFecha();
  }

  // Método para abril la ventana modal
  abrirModal = (cliente: Cliente) => {
    this.seguimientosCliente = cliente;
    this.abrir = true;
  }

  // metodo para cerrar la ventana modal
  cerrarModal = () => this.abrir = false;
}
