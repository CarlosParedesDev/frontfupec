import { Component, OnInit } from '@angular/core';

// modelos
import { GenerarReporte, GenerarReporteXlsx } from '../../models/reportes/generar-reporte-model';
import { Response } from '../../models/datos.models';
import { Seguimiento } from 'src/app/models/seguimiento.models';
import { SeguimientoReporte } from '../../models/reportes/seguimiento-reporte.models';
// servicios
import { AuthService } from '../../service/Auth/auth.service';
import { MensajesSweerAlertService } from '../../service/mensajes-sweer-alert.service';
import { ReportePdfService } from '../../service/reportes/reporte-pdf.service';
import { ReporteXlsxService } from '../../service/reportes/reporte-xlsx.service';
import { SeguimientoService } from '../../service/seguimiento.service';

@Component({
  selector: 'app-reporte-seguimiento',
  templateUrl: './reporte-seguimiento.component.html',
  styleUrls: ['./reporte-seguimiento.component.css']
})
/**
 * Generar reporte y ver datos del reporte
 */
export class ReporteSeguimientoComponent implements OnInit {

  seguimientos: Seguimiento [] = [];
  datosSeguimientoReporte: SeguimientoReporte [] = [];
  columnas: any = [];
  exportarColumnas: any = [];
  cabeceras: string [] = []
  // variables para las consultas
  campoFechaDesde: Date = new Date();
  campoFechaHasta: Date = new Date();
  // valor nivel satisfaccio
  valorNivel: number = 1;
  // <------------------ lista nivel de satisfaccion ----------------------->
  nivelSatisfaccionCliente = [
    { label: '1', value: 1 },
    { label: '2', value: 2 },
    { label: '3', value: 3 },
    { label: '4', value: 4 },
    { label: '5', value: 5 }
  ];
  // Constructor con inyección de dependencias
  constructor(
    private authService: AuthService,
    private mensajeService: MensajesSweerAlertService,
    private reportePdfService: ReportePdfService,
    private reporteXlsxService: ReporteXlsxService,
    private seguimientoService: SeguimientoService,
  ) { }
  
  ngOnInit(): void {
    //  pasamos columnas de la tabla
    this.cargarColumnasTabla();
    // pasamos fechas por inico mes y dia actual
    this.inicioRangoFechas();
    // lista de seguimientos
    this.listaReporteSeguimientos();
  }
  
  cargarColumnasTabla = (): void => {
    this.columnas = [
      { field: 'fecha',              header: 'Fecha' },
      { field: 'cliente',            header: 'Cliente' },
      { field: 'tarjeta',            header: 'Tarjeta' },
      { field: 'vendedor',           header: 'Vendedor' },
      { field: 'nivelSatisfaccion',  header: 'Satisfacción' },
      { field: 'observacion',        header: 'Observación' },
      { field: 'razonInconveniente', header: 'Inconveniente' },
      { field: 'sugerenciaCliente',  header: 'Súgerencia' },
    ];
    this.exportarColumnas = this.columnas.map((col:any) => {
      this.cabeceras.push(col.header);
      return {      
      title: col.header,
      dataKey: col.field
    }});
  }
  // Método para asignar valores iniciales a las fechas
  inicioRangoFechas = (): void => {
    this.campoFechaHasta = new Date();
    this.campoFechaDesde = new Date(this.campoFechaHasta.getFullYear(), this.campoFechaHasta.getMonth(), 1);
  }
  // para el valor a buscar de nivelSatisfaccion
  mostar = (event: any): void => this.valorNivel = event.value;
  listaReporteSeguimientos = (): void => {
    this.datosSeguimientoReporte = [];
    this.seguimientoService.reporteSeguimientosPorFechaNivel(this.campoFechaDesde, this.campoFechaHasta, this.valorNivel)
                           .subscribe( (response: Response) => {    
                            // el dato emitido va a ser de seguimiento
                            this.seguimientos =  (response.respuesta as Seguimiento[]);
                            this.cargarDatosGenerarReporte(this.seguimientos);
                           }, error => {
                             this.seguimientos = [];      
                             this.mensajeService.mensajeSweetInformacionToast('info', 'No hay datos');
                           });
  }
  // filtramos datos para el reporte a generar
  cargarDatosGenerarReporte = (seguimientos: Seguimiento []): void => {
    for (const seguimiento of seguimientos) {      
      let {fecha, nivelSatisfaccion, observacion, sugerenciaCliente, razonInconveniente} = seguimiento;
      fecha = new Date(fecha);
      let ceroM = (fecha.getMonth()+1) > 10 ? '' : '0'; 
      let cerod = (fecha.getDate()) > 10 ? '' : '0'; 
      let fechaFormato = `${cerod}${fecha.getDate()}-${ceroM}${fecha.getMonth()+1}-${fecha.getFullYear()}`;
      let cliente = seguimiento.cliente.usuario.nombre;
      let tarjeta = seguimiento.cliente.tarjeta.codigo;
      let vendedor = `${seguimiento.cliente.vendedor.nombres} ${seguimiento.cliente.vendedor.apellidos}`
      let segRepo: SeguimientoReporte = {fecha: fechaFormato, cliente, tarjeta, vendedor,nivelSatisfaccion, observacion, razonInconveniente, sugerenciaCliente }
      this.datosSeguimientoReporte.push(segRepo);
    }    
  }
  // generar pdf
  generarPdf = (): void => {
    // si no hay datos 
    if ( this.datosSeguimientoReporte.length === 0 ) {
      this.mensajeService.mensajeSweetInformacionToast('info', 'No hay datos a exportar')
      return;
    }
    let fechasRango = `${this.campoFechaDesde.getDate()}/${this.campoFechaDesde.getMonth()+1}/${this.campoFechaDesde.getFullYear()} - ${this.campoFechaHasta.getDate()}/${this.campoFechaHasta.getMonth()+1}/${this.campoFechaHasta.getFullYear()}`;
    // objeto para pasar los datos a generar el reporte
    let modeloReporte: GenerarReporte = {
                                          autor:  this.authService.usuario.name,
                                          titulo: 'Reporte Seguimientos Clientes',
                                          parametros: `Fechas: ${fechasRango}       Nivel satisfacción cliente: ${this.valorNivel}`,
                                          nombreArch:'seguimiento',
                                          columnas: this.exportarColumnas,
                                          datos: this.datosSeguimientoReporte
                                        }
    // llamamos el servicio y pasamos el objeto
    this.reportePdfService.exportPdf(modeloReporte, 65,20);
  }
  // generar excel
  generarXlxs = (): void => {
    if ( this.datosSeguimientoReporte.length === 0 ) {
      this.mensajeService.mensajeSweetInformacionToast('info', 'No hay datos a exportar')
      return;
    }
    let fechasRango = `${this.campoFechaDesde.getDate()}/${this.campoFechaDesde.getMonth()+1}/${this.campoFechaDesde.getFullYear()} - ${this.campoFechaHasta.getDate()}/${this.campoFechaHasta.getMonth()+1}/${this.campoFechaHasta.getFullYear()}`;
    // objeto para pasar los datos a generar el reporte
    let modeloReporte: GenerarReporteXlsx = {
                                              autor: this.authService.usuario.name,
                                              nombreArch: 'seguimiento',
                                              cabeceras: this.cabeceras,
                                              titulo: 'Reporte de vendedores',
                                              datos: this.datosSeguimientoReporte,
                                              parametros: {
                                                titulo: 'Rango fechas:',
                                                fechas: fechasRango,
                                                nivel: 'Nivel Satisfacción:',
                                                opcion: this.valorNivel
                                              }
                                            };
    // pasamos el objeto                                            
    this.reporteXlsxService.exportarXlsx(modeloReporte);

  }
}
