/* El archivo typesCript es el lugar donde se maneja la lógica del componente cliente-crud,
en la cuál implementamos variables, grupo de controles en un formulario, métodos,
ciclos de vida, etc, los cuales nos facilitarán el envio de la información a la vista */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

// pimeng
import {ConfirmationService} from 'primeng/api';
// models
import { Response } from '../../models/datos.models';
import { Cliente } from 'src/app/models/cliente.models';
// services
import { AuthService } from '../../service/Auth/auth.service';
import { ClienteService } from '../../service/cliente.service';
import { MensajesSweerAlertService } from '../../service/mensajes-sweer-alert.service';

@Component({
  selector: 'app-cliente-crud',
  templateUrl: './cliente-crud.component.html',
  styleUrls: ['./cliente-crud.component.css']
})

export class ClienteCrudComponent implements OnInit, OnDestroy {


  listaClientes: Cliente [] = [];
  clienteSeleccionado!: Cliente;
  paginador!: any;
  num = 0;
  // para poder cerrar subscripciones
  urlSubscription!: Subscription;
  urlSubscriptionBus!: Subscription;

  seleccionCampo!: string;
  terminoBusqueda: string = '';
  // verificar campos para busqueda
  verificarCampo: boolean = false;
  verificarTermino: boolean = false;

  // mensajes de error
  mensajeError!: string;

  // comboBox para realizar busquedas
  buscarPor = [
    {label: 'Nombre', value: 'nombre'},
    {label: 'Email', value: 'email'},
    {label: 'Cedula', value: 'cedula'}
    ];

  // modal bandera
  abrir = false;

  // Constructor con inyección de dependencias
  constructor(
    private confirmationService: ConfirmationService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    public authService: AuthService,
    private clienteService: ClienteService,
    private mensajesService: MensajesSweerAlertService
  ){
  }

  // Ciclos de vida ngOnInit
  ngOnInit(): void {
    this.listaClientesPag();
  }

  // Ciclos de vida ngOnDestroy
  ngOnDestroy(): void {
    this.cerrarSubscripcionLista();
    this.cerrarSubscripcionUrlBus();
  }

  // Método para cambiar de paginacion
  listaClientesPag = (): void => {
    // si existe otra supscripcion a params cerramos
    this.cerrarSubscripcionUrlBus();
    this.urlSubscription = this.activatedRouter.paramMap.subscribe( params => {
      // leemos y validamos el parametro url
      //@ts-ignore
      let pagina = +params.get('page');
      if ( !pagina) {
        pagina = 0;
      }
      this.num = pagina;
      // metodo del servicio para listar
      this.clienteService.listaCliente(pagina)
        .subscribe( (response: any) => {
            if (response.empty) {
              this.listaClientes = [];
              this.paginador = null;
              // cambiar la ruta de navegación
              this.router.navigateByUrl('cliente/:page');
              this.cerrarSubscripcionLista();
              return;
            }
            // el dato emitido va a ser de tipo Cliente
            this.listaClientes = (response.content as Cliente[]);
            this.paginador = response;
        }, error => {
          this.cerrarSubscripcionLista();
          if (error.status === 0 ) this.mensajeError = 'Error de conección con el servidor';
        }
      );
    });
  }

  // Método para eliminar un cliente mediante un id
  eliminarCliente = (cliente: Cliente) => {
    this.cerrarSubscripcionLista();
    this.cerrarSubscripcionUrlBus();
    // para el confirm dialog de primeng
    this.confirmationService.confirm({
        header: '¿Confirmar esta acción?',
        message: `Desea eliminar al cliente: ${cliente.usuario.usuario}`,
        icon: 'pi pi-info-circle',
        acceptLabel: 'Si',
        accept: () => {
            if( cliente ) {
              // eliminamos vendedor
              this.clienteService.borrarCliente(cliente.id)
                  .subscribe( (response: Response) => {
                    this.mensajesService.mensajeSweetInformacionToast('success', response.mensaje);
                    // pasos para ver si existe mas de un dato si ahy una busqueda
                    if ( this.terminoBusqueda.length > 0 && this.paginador.totalPages >= 1 && this.paginador.totalElements > 1 ) {
                      this.filtrarCliente();
                      return;
                    }
                    // si el array contiene un 1 se regresa al inicio
                    if ( this.paginador.totalPages === 1 && this.paginador.totalElements === 1 ) {
                      this.terminoBusqueda = '';
                      this.seleccionCampo = '';
                      this.listaClientesPag();
                      return;
                    }
                    this.listaClientesPag();
                  }, error => this.mensajesService.mensajeSweetFire('error', error, 'Acción denegada'));
            }
        }
    });
  }

  // Método para enrutar al inicio del paginador
  verificarSeleccionCampo = (): void => {
    this.verificarCampo = false;
    this.router.navigateByUrl('cliente/0');
  }

  // Método para filtrar la busqueda
  filtrarCliente = ( ): void => {
    let termino = this.terminoBusqueda.trim();
    // si existe otra supscripcion a params cerramos
    this.cerrarSubscripcionLista();
    this.cerrarSubscripcionUrlBus();
    // validamos campos de busqueda
    if ( termino.length === 0) {
      this.listaClientesPag();
      this.verificarTermino = true;
      return;
    } else if ( !this.seleccionCampo) {
      this.verificarTermino = false;
      this.verificarCampo = true;
      return;
    }
    this.verificarTermino = false;
    // busqueda para filtrar por campo
    this.urlSubscriptionBus = this.activatedRouter.paramMap.subscribe( params => {
      // leemos y validamos el parametro url 
      //@ts-ignore
      let pagina = +params.get('page');
      if ( !pagina || this.listaClientes.length <= 1 ) {
        pagina = 0;
      }
      // método del servicio para buscar
      this.clienteService.listarClienteBusqueda(this.seleccionCampo, pagina, termino)
          .subscribe( (response: Response) => {
            // el dato emitido va a ser de tipo Cliente
            this.listaClientes = (response.respuesta.content as Cliente[]);
            this.paginador = response.respuesta;
          }, error => {
            this.mensajesService.mensajeSweetInformacion('info', `No hay ${this.seleccionCampo} con : "${this.terminoBusqueda}"`);
            this.terminoBusqueda = '';
            this.seleccionCampo = '';
            this.listaClientesPag();
          });
    });
    this.cerrarSubscripcionLista();
  }

  // Método para cerrar la Subscripción
  cerrarSubscripcionLista = (): void => {
    if ( this.urlSubscription ) this.urlSubscription.unsubscribe();
  }

  // Método para cerrar la Subscripción
  cerrarSubscripcionUrlBus = (): void => {
    if ( this.urlSubscriptionBus ) this.urlSubscriptionBus.unsubscribe();
  }

  // Método para abril la ventana modal
  abrirModal(cliente: Cliente) {
    this.clienteSeleccionado = cliente;
    this.abrir = true;
  }
}
