/* El archivo typesCript es el lugar donde se maneja la lógica del componente interes,
en la cuál implementamos variables, grupo de controles en un formulario, métodos,
ciclos de vida, etc, los cuales nos facilitarán el envio de la información a la vista */
import { Component, OnInit } from '@angular/core';
// Formulario reactivo
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// Rutas
import { Router, ActivatedRoute } from '@angular/router';
// modelos
import { Interes } from 'src/app/models/interes.models';
import { Response } from '../../models/datos.models';
// services
import { MensajesSweerAlertService } from '../../service/mensajes-sweer-alert.service';
import { InteresService } from 'src/app/service/interes.service';

@Component({
  selector: 'app-agregar-interes',
  templateUrl: './agregar-interes.component.html',
  styleUrls: ['./agregar-interes.component.css']
})

export class AgregarInteresComponent implements OnInit {

  erroesBack: string [] = [];
  formSubmitted = false;
  interesForm!: FormGroup;
  interes!: Interes;
  interesSeleccionado!: Interes;

  // Constructor con inyección de dependencias
  constructor(
    private formBuider: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private interesService: InteresService,
    private mensajesService: MensajesSweerAlertService
  ) {}

  // Ciclos de vida ngOnInit
  ngOnInit(): void {
    this.buscarInteresId();
     // formBuider me permite construir un formulario creando un grupo de controles
    this.interesForm = this.formBuider.group({
      nombre: ['', [Validators.required, Validators.minLength(2)]],
      descripcion:  ['', [Validators.required, Validators.minLength(2)]]
  });
  }

  // Método donde se verifica si esta de actualizar o agregar tarjetas
  enviarDatos = (): void => {
    this.formSubmitted = true;
    this.erroesBack = [];
    const {nombre, descripcion} = this.interesForm.value; // Desestructuración De Objetos
    this.interes = {id: 0,  fechaGrabado: new Date(), eliminado: true, nombre, descripcion};
    if ( this.interesSeleccionado && this.interesForm.valid ) {
      this.actualizarInteres(this.interesSeleccionado.id);
    } else if (this.interesForm.valid) {
      this.crearInteres(this.interes);
    }
  }

  // Método para limpiar los campos del formulario
  limpiarCamposFormulario = (): void => {
      this.interesForm.reset();
      this.formSubmitted = false;
  }

  // Método para crear interes
  crearInteres = (interes: Interes) => {
      this.interesService.crearInteres(interes)
        .subscribe( response => {
            let data = response as Response;
            // mensaje de información
            this.mensajesService.mensajeSweetInformacion('success', data.mensaje);
            this.limpiarCamposFormulario();
          }, error => this.erroresBackEnd(error));
  }

  // Método para actualizar intereses
  actualizarInteres = (id: number): void => {
    this.interesService.actualizarInteres(id, this.interes)
          .subscribe( response => {
            let data = response as Response;
            // mensaje de información
            this.mensajesService.mensajeSweetInformacion('success', data.mensaje);
            this.limpiarCamposFormulario();
            this.router.navigateByUrl('interes/:page');
          }, error => this.erroresBackEnd(error));
  }

  // Método para buscar intereses por id
  buscarInteresId = (): void => {
    this.activatedRoute.paramMap
        .subscribe( params => {
            // @ts-ignore
            let id = +params.get('id');
            if ( id ) {
              this.interesService.buscarPorIdInteres(id)
                  .subscribe( (response) => {
                    // el dato emitido va a ser de tipo Interes
                    this.interesSeleccionado = (response.respuesta as Interes);
                     // Desestructuración De Objetos
                    const {nombre, descripcion } = this.interesSeleccionado;
                    this.interesForm.setValue({nombre, descripcion});
                  }, error => {
                    // si da error regresamos a la tabla
                    this.mensajesService.mensajeSweetInformacion('warning', error.error.mensaje);
                    this.router.navigateByUrl('interes/:page');
                  });
            } else {
              return;
            }
        });
  }
  
  // para gurdar los posibles erroes del back
  erroresBackEnd = (error: any): void => {
    if (error.errores) {
      this.erroesBack = error.errores;
    } else {
      this.erroesBack.push(error.mensaje);
    }
  }
}
