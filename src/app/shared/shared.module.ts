import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// modulos
import { PrimeNgModule } from '../primeNg/prime-ng.module';

// coponentes
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { PaginadorComponent } from './paginador/paginador.component';



@NgModule({
  declarations: [MenuComponent, FooterComponent, PaginadorComponent],
  // El array imports contiene los módulos que se implementará en todos los componente
  imports: [
    CommonModule,
    PrimeNgModule
  ],
  // El array exports me permite exportar los componentes.
  exports: [
    MenuComponent,
    FooterComponent,
    PaginadorComponent
  ]
})
export class SharedModule { }
