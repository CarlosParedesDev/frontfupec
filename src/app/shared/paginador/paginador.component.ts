/* El archivo typesCript es el lugar donde se maneja la lógica del componente paginador,
en la cuál implementamos variables, métodos, los cuales nos facilitarán el envio de la 
información a la vista */
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-paginador',
  templateUrl: './paginador.component.html',
  styleUrls: ['./paginador.component.css']
})
export class PaginadorComponent implements OnInit {

  // inputs
  @Input() paginador: any;
  @Input() nombreUrl!: string;
  @Input() num!: number;

  siguientePaginaTarjeta = 0;

  // Constructor con inyección de dependencias
  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  // Método para cambiar el numero del paginador
  cambiarPagina = (evento: any) => {
    // pasamos parametros por url
    if ( this.nombreUrl ) {
      this.router.navigate([`/${this.nombreUrl}/`, evento.page]);
    }
  }


}
