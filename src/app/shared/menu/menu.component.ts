/* El archivo typesCript es el lugar donde se maneja la lógica del componente menu,
en la cuál implementamos variables, ciclos de vida, etc,
los cuales nos facilitarán el envio de la información a la vista */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// tipado para el menubar
import { MenuItem } from 'primeng/api';
// modelos
import { User } from '../../models/user.model';
// serivicios
import { AuthService } from '../../service/Auth/auth.service';
import { MensajesSweerAlertService } from '../../service/mensajes-sweer-alert.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  items: MenuItem[] = [];
  datosUsuario: MenuItem [] = [];
  // Constructor
  constructor(
    private router: Router,
    private autService: AuthService,
    private mensajeService: MensajesSweerAlertService
  ) { }

  // Ciclos de vida ngOnInit
  ngOnInit(): void {
    // array de objetos que contienen etiquetas, iconos y rutas
    this.datosMenuPrincipal();
    this.datosUsuarioMenu();
  }
  datosMenuPrincipal = (): void => {
    this.items = [
      {
        label: 'Tarjeta',
        icon: 'pi pi-id-card',
        routerLink: 'tarjeta/:page'
      },
      {
        label: 'Vendedor',
        icon: 'pi pi-user-minus',
        routerLink: 'vendedor/:page'
      },
      {
        label: 'Clientes',
        icon: 'pi pi-users',
        routerLink: 'cliente/:page'
      },
      {
        label: 'Seguimiento-cliente',
        icon: 'pi pi-book',
        items: [
          {
            label: 'Seguimientos(CRUD)',
            icon: 'pi pi-file',
            routerLink: 'seguimientoCliente/:page'
          },
          {
            label: 'Interes(CRUD)',
            icon: 'pi pi-file-o',
            routerLink: 'interes/:page'
          },
          /*{
            label: 'Detalles-Clientes',
            icon: 'pi pi-file-pdf',
            routerLink: 'detalleCliente'
          },
          {
            label: 'Tarjetas a caducar',
            icon: 'pi pi-credit-card',
            routerLink: 'tarjetaCaducidad'
          }*/
        ]
      },
      {
        label: 'Reportes',
        icon: 'pi pi-clone',
        items: [
          /*{
            label: 'Observaciones',
            icon: 'pi pi-map',
            routerLink: 'observacion'
          },
          {
            label: 'Sugerencias',
            icon: 'pi pi-comments',
            routerLink: 'sugerencia'
          },*/
          {
            label: 'Seguimiento',
            icon: 'pi pi-file-pdf',
            routerLink: 'reporteSeguimiento'
          },
          {
            label: 'Vendedor',
            icon: 'pi pi-file-pdf',
            routerLink: 'vendedorCliente'
          }
        ]
       }
    ];
  }
  datosUsuarioMenu = (): void => {    
    this.datosUsuario = [
      // nombre usuario
      {label: this.usuarioGuardado().name, icon: 'pi pi-user', disabled: true },
      // rol usuario
      // @ts-ignore
      {label: this.usuarioGuardado().userLevel[0].toUpperCase(), icon: 'pi pi-shield', disabled: true},
      {label: 'Cerrar sesión', icon: 'pi pi-sign-out', command: () => this.cerrarSesion()}
  ];
  }
  // pasmos datos del usuario
  usuarioGuardado = (): User => this.autService.usuario;
  
  // cerramos session
  cerrarSesion = (): void => {
    this.autService.cerrarSesion();
    this.router.navigate(['/login']);
    this.mensajeService.mensajeSweetInformacion('info','Sesión cerrada');
  }
}
