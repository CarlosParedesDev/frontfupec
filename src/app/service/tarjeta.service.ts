/* Esta clase nos permite conectar varios sistemas basados en el protocolo HTTP
lo que nos permite realizar servicios rest en la tabla tarjeta, el cual nos
devuelve datos en formatos específicos, los servicios rest implementados en esta
clase son: GET, POST, PUT y DELETE */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

// modelos
import { Tarjeta } from '../models/tarjeta.model';
import { Response, ResponseValidacion } from '../models/datos.models';
// constante
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})

export class TarjetaService {
  // Ruta principal del entorno de producción
  urlEndPoint = environment.endPoint;
  // Constructor con inyección de dependencias
  constructor(
    private http: HttpClient
  ) { }
   // <--------------------------------- Post ---------------------------------> //
  // Método que crea una nueva Tarjeta
  crearTarjeta = ( tarjeta: Tarjeta ) => {
    return this.http.post(`${this.urlEndPoint}/tarjeta/crear`, tarjeta)
            .pipe(
              catchError( error => {
                return throwError(error.error);
              })
            );
  }
// <---------------------------------- Puts --------------------------------> //
  // Método que actualiza una tarjeta
  actualizarTarjeta = ( id: number, tarjeta: Tarjeta ) => {
    return this.http.put(`${this.urlEndPoint}/tarjeta/act/${id}`, tarjeta)
            .pipe(
              map( response => response as Response),
              catchError( error => {
                return throwError(error.error);
              })
            );
  }
     // <--------------------------------- Gets --------------------------------> //
  // Método que lista los registros de las Tarjetas mediante un paginador
  listaTarjeta = (pagina: number) => {
    return this.http.get(`${this.urlEndPoint}/tarjeta/lista/${pagina}`)
            .pipe(
              catchError((error) => {
                return throwError(error);
            })
    );
  }
// <--------------------------------- Gets --------------------------------> //
  // Método que lista los registros de las Tarjetas mediante un codigo, la lista es paginable
  listarTarjetaBusqueda = (pagina: number, codigo: string) => {
    return this.http.get(`${this.urlEndPoint}/tarjeta/buscar/${pagina}/${codigo}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
  // <--------------------------------- Gets --------------------------------> //
  // Método que busca una tarjeta mediante el id
  buscarPorIdTarjeta = (id: number) => {
    return this.http.get(`${this.urlEndPoint}/tarjeta/${id}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
    // <--------------------------------- Gets --------------------------------> //
  // Método que busca  una tarjeta mediante el codigo
  buscarPorCodigoTarjeta = (termino: string) => {
    return this.http.get(`${this.urlEndPoint}/tarjeta/codigo/${termino}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
// <---------------------------------- GET ---------------------------------> //
  // Método para eliminar un registro de una tarjeta por medio del id
  borrarTarjeta = (id: number) => {
    return this.http.delete(`${this.urlEndPoint}/tarjeta/borrar/${id}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
  
}
