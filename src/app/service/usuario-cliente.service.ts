/* Esta clase nos permite conectar varios sistemas basados en el protocolo HTTP
lo que nos permite realizar servicios rest en la tabla usuario-cliente, el cual nos
devuelve datos en formatos específicos, los servicios rest implementados en esta
clase son: GET, POST, PUT y DELETE */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

// modelos
import { Usuario } from '../models/usuario.models';
import { Response } from '../models/datos.models';
// constantes
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})

export class UsuarioClienteService {
  // Ruta principal del entorno de producción
  urlEndPoint = environment.endPoint;
  // Constructor con inyección de dependencias
  constructor(
    private http: HttpClient
  ) { }
 // <--------------------------------- Post ---------------------------------> //
  // Método que crea un nuevo Usuario
  crearUsuarioCliente = ( usuarioCliente: Usuario ) => {
    return this.http.post(`${this.urlEndPoint}/usucli/crear`, usuarioCliente)
            .pipe(
              catchError( error => {
                return throwError(error.error);
              })
            );
  }
// <---------------------------------- Puts --------------------------------> //
  // Método que actualiza un Usuario
  actualizarUsuarioCliente = ( id: number, usuarioCliente: Usuario ) => {
    return this.http.put(`${this.urlEndPoint}/usucli/act/${id}`, usuarioCliente)
            .pipe(
              map( response => response as Response),
              catchError( error => {
                return throwError(error.error);
              })
            );
  }

 // <---------------------------------- GET ---------------------------------> //
  // Método para eliminar un registro de un Usuario por medio del id
  borrarUsuarioCliente = (id: number) => {
    return this.http.delete(`${this.urlEndPoint}/usucli/borrar/${id}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error.error.mensaje);
              })
            );
  }
}
