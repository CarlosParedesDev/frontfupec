/**
 * Servicio para generar excels
 */
import { Injectable } from '@angular/core';

// librerias para generar xlsx
import { Workbook } from 'exceljs';
// @ts-ignore
import * as fs from 'file-saver';

//  modelos
import { GenerarReporteXlsx } from '../../models/reportes/generar-reporte-model';
@Injectable({
  providedIn: 'root'
})
export class ReporteXlsxService {

  exportarXlsx = (generarReporte: GenerarReporteXlsx) => {
    let fechaActual = new Date();
    let fecha = `${fechaActual.getDate()}/${fechaActual.getMonth()+1}/${fechaActual.getFullYear()}`;
    let nombreArchivo = `${generarReporte.nombreArch}_${fecha}`;
    // Creamos libro y una hoja
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet(generarReporte.titulo);
    // agregar filas y formato
    let titleRow = worksheet.addRow([generarReporte.titulo]);
    worksheet.addRow([]);
    // verificamos si tiene parametros de busqueda
    if ( generarReporte.parametros){
      let parametros = generarReporte.parametros;
      let subTitleRow = worksheet.addRow([ parametros.titulo, parametros.fechas,' ', parametros.nivel ,parametros.opcion]);
    } else {
      let subTitleRow = worksheet.addRow([ 'Fecha: ',fecha]);
    }
    // fila en blanco
    worksheet.addRow([]);
    // agregamos fila cabecera
    let headerRow = worksheet.addRow(generarReporte.cabeceras);
    // estilo de la celda y borde
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '' },
        bgColor: { argb: 'FFFFFAFA' },
        
      }
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    });
    // agregamos datos a las celdas
    for (let x1 of generarReporte.datos){
      let x2=Object.keys(x1);
      let temp=[]
      for(let y of x2)
      {
        temp.push(x1[y])
      }
      worksheet.addRow(temp)
    }
    worksheet.getColumn(1).width = 20;
    worksheet.getColumn(2).width = 20;
    worksheet.getColumn(3).width = 20;
    worksheet.getColumn(4).width = 20;
    worksheet.getColumn(5).width = 20;
    worksheet.getColumn(6).width = 20;
    worksheet.getColumn(7).width = 20;
    worksheet.getColumn(8).width = 20;
    worksheet.addRow([]);
    // footer
    let footerRow = worksheet.addRow([`Reporte generado por : ${generarReporte.autor}`]);
    footerRow.getCell(1).fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'FFCCFFE5' }
    };
    footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    // combianr celdas
    worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
    // generamos excel
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, `${nombreArchivo}.xlsx`);
    })

  }
}
