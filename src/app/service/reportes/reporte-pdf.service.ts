/**
 * Servicio para generar pdfs
 */

import { Injectable } from '@angular/core';

// librerias para generar pdfs
import { jsPDF } from 'jspdf';
import 'jspdf-autotable';

// modelos
import { GenerarReporte } from '../../models/reportes/generar-reporte-model';
@Injectable({
  providedIn: 'root'
})
export class ReportePdfService {

  exportPdf = (generarReporte: GenerarReporte, tituloX: number, tituloY: number) =>  {
    let fechaActual = new Date();
    let formatoFecha = `${fechaActual.getDate()}/${fechaActual.getMonth()+1}/${fechaActual.getFullYear()}`;
    //  creamos un documento nuevo
    const doc = new jsPDF();
    doc.setTextColor(0, 127, 255);
    // agregamos un titulo con posiciones a definir
    doc.text(`${generarReporte.titulo}`, tituloX,tituloY);         
    doc.setTextColor(0, 0, 0);
    // subtitulo
    doc.text(`${generarReporte.parametros}`,13.5,30);
    // generamos tabla de datos
    // @ts-ignore
    doc.autoTable(generarReporte.columnas, generarReporte.datos, { startY: 35});
    // para un pie de pagina con autor y numero de paginas
    // @ts-ignore
    const pageCount = doc.internal.getNumberOfPages();
    for(var i = 1; i <= pageCount; i++) {
      doc.setPage(i);
      doc.text( `Fecha: ${formatoFecha}    Autor: ${generarReporte.autor}` +'   Pagina ' + String(i) + ' de ' + String(pageCount),55-20,310-30);
    }
    // gurdamos el documento
    doc.save(`${generarReporte.nombreArch}_${formatoFecha}.pdf`);
}
}
