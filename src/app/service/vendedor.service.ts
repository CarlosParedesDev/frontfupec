/* Esta clase nos permite conectar varios sistemas basados en el protocolo HTTP
lo que nos permite realizar servicios rest en la tabla tarjeta, el cual nos
devuelve datos en formatos específicos, los servicios rest implementados en esta
clase son: GET, POST, PUT y DELETE */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

// modelos
import { Vendedor } from '../models/vendedor.model';
import { Response } from '../models/datos.models';
// constantes
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class VendedorService {
  // Ruta principal del entorno de producción
  urlEndPoint = environment.endPoint;
  // Constructor con inyección de dependencias
  constructor(
    private http: HttpClient
  ) { }
   // <--------------------------------- Post ---------------------------------> //
  // Método que crea un nuevo Vendedor
  crearVendedor = ( vendedor: Vendedor ) => {
    return this.http.post(`${this.urlEndPoint}/vendedor/crear`, vendedor)
            .pipe(
              catchError( error => {
                return throwError(error.error);
              })
            );
  }
// <---------------------------------- Puts --------------------------------> //
  // Método que actualiza un Vendedor
  actualizarVendedor = ( id: number, vendedor: Vendedor ) => {
    return this.http.put(`${this.urlEndPoint}/vendedor/act/${id}`, vendedor)
            .pipe(
              map( response => response as Response),
              catchError( error => {
                return throwError(error.error);
              })
            );
  }
   // <--------------------------------- Gets --------------------------------> //
  // Método que lista los registros de los Vendedores mediante un paginador
  listaVendedores = (pagina: number) => {
    return this.http.get(`${this.urlEndPoint}/vendedor/lista/${pagina}`)
            .pipe(
              catchError((error) => {
                return throwError(error);
            })
    );
  }

 // <--------------------------------- Gets --------------------------------> //
  // Método que lista los registros de los Vendedores mediante un comboBox, la lista es paginable
  listaVendedoresBusqueda = (pagina: number, termino: string, campo: string) => {
    let campoBusqueda = campo.toLocaleLowerCase();
    return this.http.get(`${this.urlEndPoint}/vendedor/${campoBusqueda}/${pagina}/${termino}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
   // <--------------------------------- Gets --------------------------------> //
  // Método que busca un Vendedor mediante el id
  buscarPorIdVendedor = (id: number) => {
    return this.http.get(`${this.urlEndPoint}/vendedor/${id}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
  // para filtrar en clientes agregar
   // <--------------------------- GET --------------------------> //
  // Método que lista los registros filtrados del email de los usuaros
  listaVendedoresEmail = (termino: string) => {
    return this.http.get(`${this.urlEndPoint}/vendedor/listaemail/${termino}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
  // Método que lista de vendedores con clientes
  listaVendedoresClientes = () => {
    return this.http.get(`${this.urlEndPoint}/vendedor/clientes`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
  // Método que lista de vendedores con clientes por nombre vendedor
  listaVendedoresClientesNombres = (termino: string) => {
    return this.http.get(`${this.urlEndPoint}/vendedor/clientes/nombres/${termino}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }

 // <---------------------------------- delete ---------------------------------> //
  // Método para eliminar un registro de un Vendedor por medio del id
  borrarVendedor = (id: number) => {
    return this.http.delete(`${this.urlEndPoint}/vendedor/borrar/${id}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
}
