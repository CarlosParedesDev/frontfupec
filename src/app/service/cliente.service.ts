/* Esta clase nos permite conectar varios sistemas basados en el protocolo HTTP
lo que nos permite realizar servicios rest en la tabla clientes, el cual nos
devuelve datos en formatos específicos, los servicios rest implementados en esta
clase son: GET, POST, PUT y DELETE */
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

// Modelos
import { Cliente } from '../models/cliente.models';
import { Response } from '../models/datos.models';
// Constantes
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})

export class ClienteService {
  // Ruta principal del entorno de producción
  urlEndPoint = environment.endPoint;
  // Constructor con inyección de dependencias
  constructor(
    private http: HttpClient
  ) { }

  // <--------------------------------- Post ---------------------------------> //
  // Método que crea un nuevo cliente
  crearCliente = ( cliente: Cliente ) => {
    return this.http.post(`${this.urlEndPoint}/cliente/crear`, cliente)
            .pipe(
              catchError( error => {
                return throwError(error.error);
              })
            );
  }
  // <---------------------------------- Puts --------------------------------> //
  // Método que actualiza un cliente
  actualizarCliente = ( id: number, cliente: Cliente ) => {
    return this.http.put(`${this.urlEndPoint}/cliente/act/${id}`, cliente)
            .pipe(
              map( response => response as Response),
              catchError( error => {
                return throwError(error.error);
              })
            );
  }
   // <--------------------------------- Gets --------------------------------> //
  // Método que lista los registros de los clientes mediante un paginador
  listaCliente = (pagina: number) => {
    return this.http.get(`${this.urlEndPoint}/cliente/lista/${pagina}`)
            .pipe(
              catchError((error) => {
                return throwError(error);
            })
    );
  }
 // <--------------------------------- Gets --------------------------------> //
  // Método que lista los registros de los clientes mediante un comboBox, la lista es paginable
  listarClienteBusqueda = (campoBusqueda: string, pagina: number, termino: string) => {
    return this.http.get(`${this.urlEndPoint}/cliente/${campoBusqueda}/${pagina}/${termino}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
  // <--------------------------------- Gets --------------------------------> //
  // Método que busca  un cliente mediante el id
  buscarPorIdCliente = (id: number) => {
    return this.http.get(`${this.urlEndPoint}/cliente/buscar/${id}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
  // <--------------------------- GET --------------------------> //
  // Método que lista los registros de los seguimientos mediante un paginador
  listarClientesSeguimientos = (pagina: number) => {
    return this.http.get(`${this.urlEndPoint}/cliente/seguimiento/${pagina}`)
              .pipe(
                catchError((error) => {
                  return throwError(error);
              })
    );
  }
    // <--------------------------- GET --------------------------> //
  // Método que lista los registros filtrados por cedula o nombre en la parte de agregar seguiminetos
  listarClientesFiltrado = (campoBusqueda: string, termino: string) => {
    return this.http.get(`${this.urlEndPoint}/cliente/lista/${campoBusqueda}/${termino}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
    // <--------------------------- GET --------------------------> //
  // Método que busca los registros de los cliente mediante un paginador
  listarClientesSeguiminetosBusqueda = (campoBusqueda: string, pagina: number, termino: string) => {
    return this.http.get(`${this.urlEndPoint}/cliente/seguimiento/${campoBusqueda}/${pagina}/${termino}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
  // <---------------------------------- GET ---------------------------------> //
  // Método para eliminar un registro de un cliente por medio del id
  borrarCliente = (id: number) => {
    return this.http.delete(`${this.urlEndPoint}/cliente/borrar/${id}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error.error.mensaje);
            })
    );
  }
}
