/**
 * Servicio para validar y guardar usuario autetificado
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
// estaticos
import { environment } from '../../../environments/environment.prod';
// modelo
import { User } from '../../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // Ruta principal del entorno de producción
  urlEndPoint = environment.endPointOaut;
  
  // contendran informacion del token
  private _usuario!: User;
  private _token!: string;

  usuarioNuevo!: User;
  constructor( private http: HttpClient ) { }

  // para revisar si hay un usuario en el session
  public get usuario(): User {
    if (this._usuario != null) {
      return this._usuario;
    } else if ( this._usuario == null && sessionStorage.getItem('usuario') != null ) {
      this._usuario = JSON.parse(sessionStorage.getItem('usuario') || '') as User;
      return this._usuario;
    }
    // si no hay datos en el session storage
    return this.usuarioNuevo;
  }
  // para revisar si ahy token en el session
  public get token(): string | null {
    if (this._token != null) {
      return this._token;
    } else if (this._token == null && sessionStorage.getItem('token') != null) {
      this._token = sessionStorage.getItem('token') || '';
      return this._token;
    }
    return null;
  }

  iniciarSesion = (usuario: User): Observable<any> => {
    // credenciales del cliente    
    const credenciales = btoa('fupecApp' + ':' + 'FuPeC@2021#');
    // las cabeceras de la peticion
    const httpHeaders = new HttpHeaders({
                                          'Content-Type': 'application/x-www-form-urlencoded',
                                          Authorization: 'Basic ' + credenciales,
                                        });
    // parametros de autetificacion
    let params = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('username', usuario.username);
    params.set('password', usuario.password);
    // pasmos los datos a la peticion post
    return this.http.post<any>(`${this.urlEndPoint}oauth/token`, params.toString(), { headers: httpHeaders });
  }

  // para gurdar usuarion en el session storage
  guardarUsuario = (access_token: string): void => {
    let payload = this.obtenerDatosToken(access_token);
    let {id, name, user_name, authorities} = payload;
    this._usuario = {id, username: user_name, name, password:'',userLevel:authorities};    
    // gurdamos en el sesionstorage
    sessionStorage.setItem('usuario', JSON.stringify(this._usuario));
  }

  // guradamos token en el session storage
  guardarToken = (access_token: string): void => {
    this._token = access_token;
    sessionStorage.setItem('token', this._token);
  }

  // extraemos paylaod del token si existe
  obtenerDatosToken = (access_token: string | null): any => {
    if (access_token !== null) {    
      // decodificamos el payload y paramos por un metodo para revisar caracteres especiales  
      return JSON.parse(this.decode(atob(access_token.split('.')[1])));
    }
    return null;
  }

  // verificamos si se autentifica correctamente
  isAuthenticated = (): boolean => {
    let payload = this.obtenerDatosToken(this.token);
    if (payload !== null && payload.user_name && payload.user_name.length > 0) {
      return true;
    }
    return false;
  }
  
  // cerramos session
  cerrarSesion = (): void => {
    // @ts-ignore
    this._token = null;
    // @ts-ignore
    this._usuario = null;
    // borramos de la memoria
    sessionStorage.removeItem('usuario');
    sessionStorage.removeItem('token');
  }

  // verificamos que contenga el rol permitido
  hasRole = (role: string): boolean => {
    // @ts-ignore
    if (this.usuario.userLevel[0] === role ) {        
      return true;
    }
    return false;
  };

  // para los caracteres especiales de payload
  decode = (utftext: any): string => {
    let string = "";
    let i = 0;
    let c = 0;
    let c1 = 0;
    let c2 = 0;
    let c3 = 0;
    while (i < utftext.length) {
        c = utftext.charCodeAt(i);
        if (c < 128) {
            string += String.fromCharCode(c);
            i++;
        }
        else if ((c > 191) && (c < 224)) {
            c2 = utftext.charCodeAt(i + 1);
            string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = utftext.charCodeAt(i + 1);
            c3 = utftext.charCodeAt(i + 2);
            string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }

    }
    return string;
  }

}
