/* Esta clase nos permite conectar varios sistemas basados en el protocolo HTTP
lo que nos permite realizar servicios rest en la tabla seguimientos, el cual nos
devuelve datos en formatos específicos, los servicios rest implementados en esta
clase son: GET, POST, PUT y DELETE */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

// modelos
import { Seguimiento } from '../models/seguimiento.models';
import { Response } from '../models/datos.models';
// constantes
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class SeguimientoService {
  // Ruta principal del entorno de producción
  urlEndPoint = environment.endPoint;
  // Constructor con inyección de dependencias
  constructor(
    private http: HttpClient
  ) { }
 // <--------------------------------- Post ---------------------------------> //
  // Método que crea un nuevo Seguimiento
  crearSeguimiento = (seguimiento: Seguimiento) => {
    return this.http.post(`${this.urlEndPoint}/seguimiento/crear`, seguimiento)
            .pipe(
              catchError( error => {
                return throwError(error.error);
              })
            );
  }
   // <---------------------------------- Puts --------------------------------> //
  // Método que actualiza un Seguimientos
  actualizarSeguimiento = ( id: number, seguimiento: Seguimiento ) => { 
    return this.http.put(`${this.urlEndPoint}/seguimiento/act/${id}`, seguimiento)
            .pipe(
              map( response => response as Response),
              catchError( error => {
                return throwError(error.error);
              })
            );
  }
// <---------------------------------- GET ---------------------------------> //
  // Método para eliminar un registro de un Seguimientos por medio del id
  borrarSeguimiento = (id: number) => {

    return this.http.delete(`${this.urlEndPoint}/seguimiento/borrar/${id}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error.error.mensaje);
              })
            );
  }
     // <----------------------- Gets ---------------------> //
  // Método para listar Seguimientos paginable
  listaSeguimientos = (pagina: number) => {
    return this.http.get(`${this.urlEndPoint}/seguimiento/lista/${pagina}`)
            .pipe(
              catchError((error) => {
                return throwError(error);
            })
            );
  }

 // <--------------------------------- Gets --------------------------------> //
  // Método que busca  un Segumiento mediante el id
  buscarPorIdSeguimiento = (id: number) => {
    return this.http.get(`${this.urlEndPoint}/seguimiento/buscar/${id}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
            );
  }

  // <--------------------------- GET --------------------------> //
  // Método que lista los registros por rango de fecha
  buscarSeguimientoPorFecha = (campo: string, pagina: number, fechaDesde: Date, fechaHasta: Date) => {
   return this.http.get(`${this.urlEndPoint}/seguimiento/buscar/${campo}/${pagina}/${fechaDesde}/${fechaHasta}`)
           .pipe(
             map( response => response as Response),
             catchError((error) => {
               return throwError(error);
           })
           );
  }
  // Método que lista los registros por rango de fecha y nivel satisfaccion reporte
  reporteSeguimientosPorFechaNivel = (fechaDesde: Date, fechaHasta: Date, nivelSatisfaccion: number) => {
    return this.http.get(`${this.urlEndPoint}/seguimiento/reporte/${fechaDesde}/${fechaHasta}/${nivelSatisfaccion}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
            );
   }
}
