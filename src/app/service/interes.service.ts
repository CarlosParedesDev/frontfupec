/* Esta clase nos permite conectar varios sistemas basados en el protocolo HTTP
lo que nos permite realizar servicios rest en la tabla interes, el cual nos
devuelve datos en formatos específicos, los servicios rest implementados en esta
clase son: GET, POST, PUT y DELETE */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

// modelos
import { Interes } from '../models/interes.models';
import { Response } from '../models/datos.models';
// constantes
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class InteresService {
  // Ruta principal del entorno de producción
  urlEndPoint = environment.endPoint;
  // Constructor con inyección de dependencias
  constructor(
    private http: HttpClient
  ) { }
 // <--------------------------------- Post ---------------------------------> //
  // Método que crea un nuevo Interes
  crearInteres = (interes: Interes) => {
    return this.http.post(`${this.urlEndPoint}/interes/crear`, interes)
            .pipe(
              catchError( error => {
                return throwError(error.error);
              })
            );
  }
  // <---------------------------------- Puts --------------------------------> //
  // Método que actualiza un Interes
  actualizarInteres  = ( id: number, interes: Interes ) => {
    return this.http.put(`${this.urlEndPoint}/interes/act/${id}`, interes)
            .pipe(
              map( response => response as Response),
              catchError( error => {
                return throwError(error.error);
              })
            );
  }
// <---------------------------------- GET ---------------------------------> //
  // Método para eliminar un registro de un Interes por medio del id
  borrarInteres  = (id: number) => { 

    return this.http.delete(`${this.urlEndPoint}/interes/borrar/${id}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
              })
            );
  }
    // <--------------------------- GET --------------------------> //
  // Método que lista los registros de los Intereses mediante un paginador
  listaInteres  = (pagina: number) => {
    return this.http.get(`${this.urlEndPoint}/interes/lista/${pagina}`)
            .pipe(
              catchError((error) => {
                return throwError(error);
            })
    );
  }

 // <--------------------------------- Gets --------------------------------> //
  // Método que busca  un Interes mediante el id
  buscarPorIdInteres  = (id: number) => { 
     return this.http.get(`${this.urlEndPoint}/interes/buscar/${id}`)
             .pipe(
               map( response => response as Response),
               catchError((error) => {
                 return throwError(error);
             })
     );
  }
  // <--------------------------------- Gets --------------------------------> //
  // Método que lista los registros de los Intereses mediante el nombre del interes, la lista es paginable
  listarInteresBusqueda = ( pagina: number, termino: string) => {
    return this.http.get(`${this.urlEndPoint}/interes/nombre/${pagina}/${termino}`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }

    // <--------------------------------- Gets --------------------------------> //
  // Método que lista los registros de los intereses mediante un paginador
  listarIntereses = () => {
    return this.http.get(`${this.urlEndPoint}/intereses`)
            .pipe(
              map( response => response as Response),
              catchError((error) => {
                return throwError(error);
            })
    );
  }
}
